<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function() {
    Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
    Route::get('/profilesettings','App\Http\Controllers\HomeController@profilesettings')->name('profilesettings');
    Route::post('/updateprofile','App\Http\Controllers\HomeController@updateprofilesettings')->name('updateprofile');
    Route::group(['middleware'=>['role:admin']],function(){

    Route::resource('category' , 'App\Http\Controllers\CategoryController');

    Route::get('/cms','App\Http\Controllers\CmsController@index')->name('cms');
    Route::post('/updateaboutus','App\Http\Controllers\CmsController@updateaboutussection')->name('updateaboutus');
    Route::post('/updatetermsandcondition', 'App\Http\Controllers\CmsController@updatetermsandconditionsection')->name('updatetermsandcondition');
    Route::post('/updatewelcomevideo', 'App\Http\Controllers\CmsController@updatewelcomevideosection')->name('updatewelcomevideo');
    Route::get('/adduser', 'App\Http\Controllers\UserController@create')->name('createuser');
    Route::post('/saveuser', 'App\Http\Controllers\UserController@store')->name('saveuser');
    Route::get('/userslist','App\Http\Controllers\UserController@index')->name('userslist');
    Route::get('/userdetail/{id}','App\Http\Controllers\UserController@viewuserdetail')->name('userdetail');
    Route::post('/updateuser', 'App\Http\Controllers\UserController@updateuserdetail')->name('updateuser');
    Route::get('/paymentsettings','App\Http\Controllers\HomeController@paymentsettings')->name('paymentsettings');
    Route::post('/updatepaymentsettings', 'App\Http\Controllers\HomeController@updatepaymentsettingssection')->name('updatepaymentsettings');
    Route::post('/updatecreditpackages','App\Http\Controllers\HomeController@updatecreditpackagessection')->name('updatecreditpackages');
    Route::get('/customerjob/{id}','App\Http\Controllers\UserController@customerjobdetail')->name('customerjob');
    Route::get('/deleteestablishmentimagebyadmin/{id}','App\Http\Controllers\UserController@deleteestablishmentimagebyadmin')->name('deleteestablishmentimagebyadmin');
    Route::get('/supplierjob/{id}','App\Http\Controllers\UserController@supplierjobdetail')->name('supplierjob');




    });
    Route::group(['middleware'=>['role:customer']],function(){
    Route::get('/newservice','App\Http\Controllers\ServiceController@index')->name('newservice');
    Route::post('/savejobrequest','App\Http\Controllers\ServiceController@savecustomerjob')->name('savejobrequest');
    Route::get('/customerinbox','App\Http\Controllers\ServiceController@customerinbox')->name('customerinbox');
    //Route::get('/customerjob','App\Http\Controllers\UserController@customerjobdetail')->name('customerjob');
    Route::get('/specificjobrequests/{id}','App\Http\Controllers\ServiceController@specificjobrequests')->name('specificjobrequests');
    Route::get('/viewsupplierprofile/{id}/{jobid}', 'App\Http\Controllers\ServiceController@specificsupplierforjobdetail')->name('viewsupplierprofile');
    Route::get('/awardjob/{jobid}/{supplierid}', 'App\Http\Controllers\ServiceController@awardjobtosupplier')->name('awardjob');
    Route::get('/jobdetail','App\Http\Controllers\ServiceController@jobdetail')->name('jobdetail');
    Route::post('/givereview','App\Http\Controllers\ServiceController@giveSupplierReview')->name('givereview');


    });
    Route::group(['middleware'=>['role:supplier']],function(){
        Route::get('/myjobdetail/{id}','App\Http\Controllers\ServiceController@myjobdetail')->name('myjobdetail');
        Route::get('/acceptjobDetail/{id}','App\Http\Controllers\ServiceController@acceptthisjob')->name('acceptjobDetail');
        Route::get('/reviewPendingjobDetail/{id}','App\Http\Controllers\ServiceController@permissiontoreviewthisjob')->name('reviewPendingjobDetail');
        Route::get('/buycredits','App\Http\Controllers\ServiceController@buycredits')->name('buycredits');
        Route::get('/addcredits/{id}', 'App\Http\Controllers\ServiceController@addcredits')->name('addcredits');
        Route::get('/jobdetail','App\Http\Controllers\UserController@detailforsupplier')->name('jobdetail');
        Route::get('/supplierestablishments','App\Http\Controllers\ServiceController@supplierestablishments')->name('supplierestablishments');
        Route::post('/addsupplierestablishment', 'App\Http\Controllers\ServiceController@savesupplierestablishment')->name('addsupplierestablishment');
        Route::get('/establishmentimages','App\Http\Controllers\ServiceController@establishmentimages')->name('establishmentimages');
        Route::post('/saveestablishmentimages', 'App\Http\Controllers\ServiceController@saveestablishmentimages')->name('establshmentimages.store');
        Route::post('/updatesupplierdetail','App\Http\Controllers\ServiceController@updatesupplierestablishment')->name('updatesupplierestablishment');
        Route::get('/supplierinbox','App\Http\Controllers\ServiceController@supplierinbox')->name('supplierinbox');
        Route::get('/deleteestablishmentimage/{id}','App\Http\Controllers\ServiceController@deleteestablishmentimage')->name('deleteestablishmentimage');

    });
    /* Route::get('/customerjob','App\Http\Controllers\UserController@customerjobdetail')->name('customerjob');
    Route::get('/jobdetail','App\Http\Controllers\UserController@detailforsupplier')->name('jobdetail');
    Route::get('/cms','App\Http\Controllers\CmsController@index')->name('cms');
    Route::get('/newservice','App\Http\Controllers\ServiceController@index')->name('newservice');
    Route::get('/customerinbox','App\Http\Controllers\ServiceController@customerinbox')->name('customerinbox');
    Route::get('/specificjobrequests','App\Http\Controllers\ServiceController@specificjobrequests')->name('specificjobrequests');
    Route::get('/jobdetail','App\Http\Controllers\ServiceController@jobdetail')->name('jobdetail');
    Route::get('/supplierinbox','App\Http\Controllers\ServiceController@supplierinbox')->name('supplierinbox');
    Route::get('/myjobdetail','App\Http\Controllers\ServiceController@myjobdetail')->name('myjobdetail');
    Route::get('/paymentsettings','App\Http\Controllers\HomeController@paymentsettings')->name('paymentsettings');
    Route::get('/profilesettings','App\Http\Controllers\HomeController@profilesettings')->name('profilesettings');
    Route::get('/buycredits','App\Http\Controllers\ServiceController@buycredits')->name('buycredits');
    Route::get('/supplierestablishments','App\Http\Controllers\ServiceController@supplierestablishments')->name('supplierestablishments');
    Route::get('/establishmentimages','App\Http\Controllers\ServiceController@establishmentimages')->name('establishmentimages'); */
    });


