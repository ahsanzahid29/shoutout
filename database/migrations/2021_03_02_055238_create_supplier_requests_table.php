<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSupplierRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('supplier_requests', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('request_id');
            $table->unsignedBigInteger('user_id');
            $table->unsignedInteger('status')->default(0);
            $table->unsignedInteger('is_reviewed')->default(0);
            $table->dateTime('sent_dateandtime')->nullable();
            $table->dateTime('accept_dateandtime')->nullable();
            $table->date('award_dateandtime')->nullable();
            $table->softDeletes();
            $table->timestamps();
            $table->foreign('request_id')->references('id')->on('customer_requests')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('supplier_requests');
    }
}
