<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class CreditsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $credits = [
            [
                'id' => 1,
                'creditname' => 'First Credit Package',
                'creditprice' => 100,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], [
                'id' => 2,
                'creditname' => 'Second Credit Package',
                'creditprice' => 150,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            [
                'id' => 3,
                'creditname' => 'Third Credit Package',
                'creditprice' => 200,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ],
            ];
        \DB::table('category_packages')->insert($credits);
    }
}
