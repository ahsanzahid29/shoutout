<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class CmsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $cms = [

                'id' => 1,
                'aboutusvideo' => 'https://www.youtube.com/watch?v=XZDA2XrwenY',
                'aboutusdescription' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. ',
                'termsvideo' => 'https://www.youtube.com/watch?v=3sCXC71Azzk',
                'termsdescription' => 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.',
                'extravideo' => 'https://www.youtube.com/watch?v=DWGQkB90wT8',
                'merchantid' => '16534001',
                'merchantkey' => 'zw2dh6jsjn0pt',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
        ];
        \DB::table('cms')->insert($cms);
    }
}
