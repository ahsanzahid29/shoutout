<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'id' => 1,
                'name' => 'admin',
                'guard_name'=>'web',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], [
                'id' => 2,
                'name' => 'customer',
                'guard_name'=>'web',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ], [
                'id' => 3,
                'name' => 'supplier',
                'guard_name'=>'web',
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]
        ];
        \DB::table('roles')->insert($roles);
        $roleAccess=[
            'role_id' => 1,
            'model_type' => 'App\Models\User',
            'model_id' => 1
        ];
        \DB::table('model_has_roles')->insert($roleAccess);
    }
}
