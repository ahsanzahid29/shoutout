<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            RoleSeeder::class,
            RegionSeeder::class,
            UserSeeder::class,
            CmsSeeder::class,
            CreditsSeeder::class

        ]);
    }
}
