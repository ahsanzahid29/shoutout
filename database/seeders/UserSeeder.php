<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = [
            'id' => 1,
            'region_id' => 1,
            'name' => 'Ahsan Zahid',
            'email' => 'ahsanzahid29@hotmail.com',
            'password' => Hash::make('password'),
            'org_password' => 'password',
            'is_supplier' => 0,
            'noofcredits' => 0,
            'random_no' => 0,
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now()
        ];
        \DB::table('users')->insert($user);
    }
}
