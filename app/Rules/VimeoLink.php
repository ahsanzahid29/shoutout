<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class VimeoLink implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $parsed = parse_url($value);
        $host = $parsed['host'];
        if($host){
            if($host=='vimeo.com'){
                return true;
            }
            else{
                return false;
            }
        }

    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Not a valid Vimeo URL.';
    }
}
