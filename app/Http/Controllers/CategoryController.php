<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Http\Requests\CategorySaveRequest;
use App\Http\Requests\CategoryUpdateRequest;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        return view('category.list',compact('category'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $category = Category::select('*')
                            ->where('status', true)
                            ->get();
        return view('category.add',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategorySaveRequest $request)
    {
       try{
           DB::beginTransaction();
           if($request->hasFile('categoryimage'))
           {
               $path = $request->file('categoryimage')->store('public/category');
           }
           if($path)
           {
               $data = [
                   'name' =>$request->name,
                   'category_id' => ($request->category_id==0) ? NULL : $request->category_id ,
                   'categoryimage' => $path,
                   'status' => $request->status
               ];
               Category::create($data);
               DB::commit();
               Toastr::success('Category Added Successfully','Success');
               return redirect('/category');
           }
       }
       catch (\Exception $e)
       {
           $res=$e->getMessage();

           DB::rollback();
           Toastr::error('Something went wrong. Please try again','Danger');
           return redirect('/category');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::select('*')
            ->where('status', true)
            ->get();
       $categories = Category::find($id);

       return view ('category.edit',compact('categories', 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdateRequest $request, $id)
    {
        $category = Category::find($id);
        $category->name = $request->name;
        $category->category_id = ($request->category_id==0) ? NULL : $request->category_id;
        $category->status = $request->status;
        if ($request->hasFile('categoryimage')) {
            $path = $request->file('categoryimage')->store('public/category');
            $category->categoryimage = $path;
        }
        else{
            $category->categoryimage = $request->oldimage;
        }
        $category->save();
        Toastr::success('Category Updated Successfully','Success');
        return redirect('/category');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::where('category_id', '=', $id)->exists();
        if($category)
        {
            Toastr::error('Parent Category. Cannot be deleted','Alert');
            return redirect('/category');
        }
        else{
            $category = Category::find($id);
            Storage::delete($category->categoryimage); //delete image from storage
            $category->delete();
            Toastr::error('Category deleted Successfully','Alert');
            return redirect('/category');
        }


    }
}
