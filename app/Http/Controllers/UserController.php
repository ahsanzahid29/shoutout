<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use DB;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;
use Illuminate\Support\Facades\Hash;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Storage;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
       $userType = $request->usertype;
       $userName = $request->username;
       $userStaus = $request->userstatus;
        $user = DB::table('users')
            ->select('users.id as id','users.name as name','users.created_at as created_at',
            'users.status as status','users.noofcredits as noofcredits')
            ->join('model_has_roles','users.id','=','model_has_roles.model_id')
            ->where('id','!=',1);

       if($userType!=0)
       {
           $user = $user->where('model_has_roles.role_id', $userType);

       }
       if($userName!=''){
           $user = $user->where('name', 'like', "%{$userName}%");

       }
       if($userStaus!=0){
           if($userStaus==1){
          $user = $user->where('status',true);

           }
           if($userStaus==2){
           $user = $user->where('status',false);
           }
       }
       $user = $user->orderBy('users.id','desc')->get();

        return view ('user.list' , compact ('user'));
    }

    function viewuserdetail($id)
    {
       $user = User::find($id);
       //check either user is customer or supplier
        $check = DB::table('model_has_roles')
                ->select('model_has_roles.role_id as userRole')
                ->where('model_has_roles.model_id',$id)
                ->first();
        $userRole = $check->userRole;
       try {
            $decrypted = Crypt::decrypt($user->org_password,false);
        } catch (DecryptException $e) {
           $decrypted = $e->getMessage();
        }
        // user inbox
       if($userRole==2)
       {
           $myJobs = DB::table('customer_requests')->select('id','description','status','created_at')->where('user_id',$id)->get();
           $countmyJobs = DB::table('customer_requests')->select('id','description','status','created_at')->where('user_id',$id)->count();
           $countRequestReceived =  DB::table('customer_requests as a')->join('supplier_requests as b','a.id','=','b.request_id')
               ->select('b.id as recordid')->where(['a.user_id'=>$id,'b.status'=>1])->count();
           $countRequestAwarded =  DB::table('customer_requests as a')->join('supplier_requests as b','a.id','=','b.request_id')
               ->select('b.id as recordid')->where(['a.user_id'=>$id,'b.status'=>2])->count();
           $countCustomerOpenJobs = DB::table('customer_requests')->select('id','description','status','created_at')->where(['user_id'=>$id,'status'=>1])->count();
           $countCustomerCompletedJobs = DB::table('customer_requests')->select('id','description','status','created_at')->where(['user_id'=>$id,'status'=>0])->count();
           return view ('user.userdetail',compact('user' , 'decrypted','userRole' , 'myJobs' ,'countmyJobs' ,'countRequestReceived', 'countRequestAwarded','countCustomerOpenJobs','countCustomerCompletedJobs'));

       }
       elseif($userRole == 3){
           $myCredits = DB::table('users')->select('noofcredits')->where('id',$id)->first();
           $totalReviews = DB::table('supplier_reviews')->where('user_id' , $id)->count();
           $awardedJobs  = DB::table('supplier_requests')->where([
               ['user_id' ,'=',$id],
               ['status','>',1]
           ])->count();
           $completedJobs = DB::table('supplier_requests')->where([
               ['user_id' ,'=',$id],
               ['status','=',4]
           ])->count();
           $supplierEstablishmentDetail = DB::table('supplier_establishments as a')
                                         ->join('regions as b','a.region_id','=','b.id')
                                         ->join('categories as c','a.cat_id','=','c.id')
                                         ->select('a.name as suppliername','a.email as supplieremail',
                                                  'a.contactnumber as suppliercontact', 'a.address as supplieraddress',
                                             'a.about as aboutsupplier','c.name as suppliercategory','b.name as regionname')
                                         ->where('a.user_id',$id)
                                         ->first();
           $supplierImages = DB::table('supplier_establishment_images')->where('user_id',$id)->get();
           $supplierjobs   = DB::table('supplier_requests as a')->join('customer_requests as b','a.request_id','=','b.id')
               ->select('b.id as jobid','b.description as jobdescription','a.status as jobstatus','b.created_at as jobdate')
               ->where('a.user_id',$id)
               ->get();

           return view ('user.userdetail',compact('user' , 'userRole' , 'decrypted' , 'myCredits' , 'totalReviews' ,'awardedJobs' , 'completedJobs' ,'supplierEstablishmentDetail' ,'supplierImages' ,'supplierjobs' ));

       }
    }
    // Admin remove supplier establishment image
    function deleteestablishmentimagebyadmin($id){
        $establishmentImage = DB::table('supplier_establishment_images')->where('id',$id)->first();
        Storage::delete($establishmentImage->image); //delete image from storage
        DB::table('supplier_establishment_images')->where('id', '=', $id)->delete();
        Toastr::error('Establishment Image is deleted','Alert');
        return redirect('/userdetail/'.$establishmentImage->user_id);

    }
    // functionality to update user settings when admin goes to user detail page
    function updateuserdetail(Request $request){
        $this->validate(
            $request,[
                'name' => 'required',
                'password' => 'nullable|min:6',
                'userimage' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048'
            ],
            [
                'name.required' =>    'Please give user name',
                'password.min'  =>    'Password must be 6 digits long',
                'userimage.image' =>  'Please upload image only',
                'userimage.mimes' =>  'Please upload jpeg,png,jpg,gif,svg images .',
                'userimage.max'   =>  'Image should be less than 3MB'
            ]

        );
        $userId = $request->user_id;
        try{
            $user = User::find($userId);
            DB::beginTransaction();
            // Upload image if any
            if($request->hasFile('userimage')){
                $path = $request->file('userimage')->store('public/userimages');
            }
            $user->name = $request->name;
            $user->status = $request->status;
            if($user->password){
                $user->password = Hash::make($request->password);
                $user->org_password = Crypt::encryptString($request->password);
            }
            if($request->hasFile('userimage')){
                $user->profileimage = $path;
            }
            $user->save();
            DB::commit();
            Toastr::success('User Detail updated successfully', 'Success');
            return redirect('/userdetail/'.$userId);
        }
        catch(\Exception $e){
            DB::rollBack();
            Toastr::error($e->getMessage(), 'Error');
            return redirect('/userdetail/'.$userId);

        }


    }
    function customerjobdetail($id)
    {
       $detail = DB::table('customer_requests as a')
                 ->join('users as b','a.user_id','=','b.id')
                 ->join ('categories as c','a.cat_id','=','c.id')
                 ->select('a.*','c.name as category','b.name as customername','b.email as customeremail')
                 ->where('a.id',$id)
                ->first();
       $newRequests = DB::table('supplier_requests as a')
                     ->join('supplier_establishments as b' , 'a.user_id','=','b.user_id')
                     ->select('b.user_id as supplierid','b.name as suppliername','b.rating as supplierrating')
                     ->where(['a.request_id'=> $id,'a.status'=> 1 ])
                     ->get();
        $awardRequests = DB::table('supplier_requests as a')
            ->join('supplier_establishments as b' , 'a.user_id','=','b.user_id')
            ->select('b.user_id as supplierid','b.name as suppliername','b.rating as supplierrating')
            ->where([
            ['a.request_id', '=', $id],
            ['a.status', '>', 1],

        ])->get();

        return view ('user.customerjobdetailwithrequest', compact('detail', 'newRequests', 'awardRequests'));
    }
    function supplierjobdetail($jobid)
    {

        $detail = DB::table('customer_requests as a')
            ->join('users as b','a.user_id','=','b.id')
            ->join ('categories as c','a.cat_id','=','c.id')
            ->select('a.*','c.name as category','b.name as customername','b.email as customeremail')
            ->where('a.id',$jobid)
            ->first();
        $acceptedRequests = DB::table('supplier_requests as a')
            ->join('supplier_establishments as b' , 'a.user_id','=','b.user_id')
            ->select('b.user_id as supplierid','b.name as suppliername','b.rating as supplierrating')
            ->where(['a.request_id'=> $jobid,'a.status'=> 1 ])
            ->get();
        $completedRequests = DB::table('supplier_requests as a')
            ->join('supplier_establishments as b' , 'a.user_id','=','b.user_id')
            ->join('supplier_reviews as c' , 'a.user_id','=','c.user_id')
            ->select('b.user_id as supplierid','b.name as suppliername','b.rating as supplierrating',
            'c.rating as rating','c.review as review')
            ->where([
                ['a.request_id', '=', $jobid],
                ['a.status', '=', 4],

            ])->get();


        return view ('user.supplierdetailwithrequest', compact('detail', 'acceptedRequests', 'completedRequests'));
    }
    function detailforsupplier()
    {
        return view ('user.specificjobdetail');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $role = DB::table('roles')
            ->where('id','!=', 1)
            ->get();
        return view ('user.adduser',compact ('role'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        DB::beginTransaction();
        $data=[
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make('password'),
            'org_password' =>Crypt::encryptString('password'),
            'region_id' => 1,
            'is_supplier' =>0,
            'random_no' => rand(10,9999)
        ];
        $result = User::create($data);
        $userId = $result->id;
        #------------- insert user id in model_has_roles table---------------------#
        DB::table('model_has_roles')->insert([
            'role_id' => $request->role_id,
            'model_type' =>'App\Models\User',
            'model_id' => $userId
        ]);
        DB::commit();
        Toastr::success('User Added Successfully','Success');
        return redirect('/userslist');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
