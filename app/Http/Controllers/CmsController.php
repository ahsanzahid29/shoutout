<?php

namespace App\Http\Controllers;

use App\Models\Cms;
use Illuminate\Http\Request;
use App\Http\Requests\AboutUsRequest;
use App\Http\Requests\TermsAndConditionRequest;
use App\Http\Requests\WelcomeRequest;
use Brian2694\Toastr\Facades\Toastr;

class CmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id=1;
        $cms = Cms::whereId($id)->first();
        return view ('cms.cmstabs' , compact('cms'));
    }

    /**
     * Store updated response for about us.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function updateaboutussection(AboutUsRequest $request)
    {
        $aboutus = Cms::find($request->recordid);
        $aboutus->aboutusvideo = $request->aboutusvideo;
        $aboutus->aboutusdescription = $request->aboutusdescription;
        $aboutus->save();
        Toastr::success('About Us Updated Successfully','Success');
        return redirect('/cms');
    }
    /**
     * Store updated response for Terms and condition.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function updatetermsandconditionsection(TermsAndConditionRequest $request)
    {
        $cms = Cms::find($request->recordid);
        $cms->termsvideo = $request->termsvideo;
        $cms->termsdescription = $request->termsdescription;
        $cms->save();
        Toastr::success('Terms and conditions updated Successfully','Success');
        return redirect('/cms');

    }
    /**
     * Store updated response for Extra video.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function updatewelcomevideosection(WelcomeRequest $request)
    {
        $cms = Cms::find($request->recordid);
        $cms->extravideo = $request->extravideo;
        $cms->save();
        Toastr::success('Extra video URL is updated Successfully','Success');
        return redirect('/cms');


    }


}
