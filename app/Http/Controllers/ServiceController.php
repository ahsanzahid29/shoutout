<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Region;
use App\Models\Service;
use App\Models\SupplierEstablishment;
use App\Models\SupplierEstablishmentImages;
use App\Models\CustomerRequest;
use App\Models\SupplierReview;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AddEstablishmentRequest;
use App\Http\Requests\UpdateSupplierEstablishmentRequest;
use App\Http\Requests\AddServiceRequest;
use DB;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Storage;
use Stripe\ApiOperations\Update;

class ServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = DB::table('categories as a')
            ->select ('a.id as cat_id','a.name as cat_name')
            ->where('a.status', true)
            ->whereNotExists(function ($query) {
                $query->select(DB::raw("b.id as subid"))
                    ->from('categories as b')
                    ->whereColumn('b.category_id', 'a.id');
            })
            ->get();

        return view('service.addnewservice',compact('categories'));
    }
    /**
     * save newly created job by customer.
     *
     *
     */
    function savecustomerjob(AddServiceRequest $request){
        //dd($request);
        $userId = Auth::user()->id;
        $geocode=file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$request->latitude.','.$request->longitude.'&sensor=false&key=AIzaSyDE19uW2_YIeuZRC3IYFl1cMZ4QYSrqkbE&');
        $output= json_decode($geocode);
        $city = $output->results[0]->address_components[3]->long_name;
        if($city){
            try {

                DB::beginTransaction();
                $data = [
                    'user_id' => $userId,
                    'cat_id' =>  $request->cat_id,
                    'budget' => $request->budget,
                    'timeframe' => $request->timeframe,
                    'area' =>$request->area,
                    'city' => $city,
                    'description' => $request->description,
                    'status' => 1, // 1 means that the job is open
                ];

                $record = CustomerRequest::create($data);
                $insertedId = $record->id;
                DB::commit();
                //get suppliers that has establishment in this city
                $result = DB::table('supplier_establishments as a')
                         ->join('users as b','a.user_id','=','b.id')
                         ->select('a.user_id as supplierId')
                         ->where("a.cat_id", $request->cat_id)
                         ->where("a.suppliercity", "like", "%{$city}%")
                         ->where([
                             ["a.suppliercity", "like", "%{$city}%"],
                             ["a.cat_id", "=", $request->cat_id],
                         ])->get();
                /*echo $city.'<br/>'.$request->cat_id;
                dd($result);*/
                if(count($result)>0){
                    foreach($result as $row){
                        $suppliers[] = $row->supplierId;
                    }
                    for($i=0;$i<count($suppliers);$i++){
                        DB::table('supplier_requests')->insert([
                            'request_id' => $insertedId,
                            'user_id' => $suppliers[$i],
                            'status' => 0 // 0 means that request sent to supplier
                        ]);
                    }

                }
                Toastr::Success('Job added. '.count($result).' supplier(s) found', 'Success');
                return redirect('/customerinbox');
            }catch (\Exception $e)
            {
                $res=$e->getMessage();
                DB::rollback();
                Toastr::error($res,'Danger');
                return redirect('/newservice');
            }

        }

    }
    /**
     * Display all listing of jobs which customer created.
     *
     *
     */
    public function customerinbox()
    {
        $userId = Auth::user()->id;
        $detail = DB::table('customer_requests as a')
                 ->join('supplier_requests as b','a.id','=','b.request_id','left')
                 ->join('categories as c' ,'a.cat_id','=','c.id')
                  ->select('a.description as jobdescription','a.id as jobid','a.status as jobstatus',
                  'a.created_at as jobcreation','c.name as category')
                  ->where('a.user_id',$userId)
                  ->orderBy('a.id','DESC')
                  ->get();

        return view ('service.customer.customerinbox' , compact('detail' ));

    }
    /**
     * Display new and awarded suppliers for a particular job.
     *
     *
     */
    public function specificjobrequests($id)
    {
        $supplierDetail = DB::table('supplier_requests as a')
                         ->join('customer_requests as b','a.request_id','=','b.id')
                         ->join('supplier_establishments as c','a.user_id','=','c.user_id')
                         ->select('c.name as suppliername','c.rating as supplierrating',
                         'a.status as supplierjobstatus','c.user_id as supplier_id')
                         ->where(['a.request_id'=>$id,'a.status'=>1])
                         ->get();
        $awardSuplierDetail = DB::table('supplier_requests as a')
            ->join('customer_requests as b','a.request_id','=','b.id')
            ->join('supplier_establishments as c','a.user_id','=','c.user_id')
            ->select('c.name as suppliername','c.rating as supplierrating',
                'a.status as supplierjobstatus','c.user_id as supplier_id')
            ->where([
                ['a.request_id', '=', $id],
                ['a.status', '>', 1]
            ])
            ->get();

        $jobid = $id;

        return view ('service.customer.specificjobrequests' , compact('supplierDetail' , 'jobid' ,'awardSuplierDetail'));

    }
    /**
     * Display supplier profile for customer alongwith job statuses which customer can change.
     *
     *
     */
    public function specificsupplierforjobdetail($supplierid,$jobid)
    {

        $supplierImages = DB::table('supplier_establishment_images')
                        ->select('image as supplierimage')
                        ->where('user_id',$supplierid)
                        ->get();
        $supplierEstdDetail = DB::table('supplier_establishments as a')
                              ->join('categories as b','a.cat_id','=','b.id')
                              ->join('regions as c','a.region_id','=','c.id')
                              ->select('a.*','b.name as category','c.name as region')
                              ->where('a.user_id',$supplierid)
                              ->first();

        $counter = 0;
        $jobDetail = DB::table('customer_requests as a')
                    ->select('a.*','b.name as cat_name')
                    ->join('categories as b','a.cat_id','=','b.id')
                    ->where(['a.id'=>$jobid,'a.user_id'=>Auth::user()->id])
            ->first();
        $jobStatus = DB::table('supplier_requests')
                     ->select('status')
                     ->where(['request_id'=>$jobid,'user_id'=>$supplierid])
                     ->first();
        if($jobDetail) {
            return view('service.customer.jobdetail', compact('supplierImages', 'counter', 'jobDetail' ,'supplierEstdDetail' ,'jobStatus'));
        }else{
            Toastr::error('Something went wrong.','Danger');
            return redirect('/customerinbox');

        }

    }
    /**
     * Award Job to Supplier
     *
     *
     */
    function awardjobtosupplier($jobid,$supplierid){
        try{
            $affected = DB::table('supplier_requests')
                ->where(['request_id'=>$jobid,'user_id'=>$supplierid])
                ->update(['status' => 2,'award_dateandtime'=>date('Y-m-d H:i')]);
            if($affected) {
                Toastr::Success('Job Awarded', 'Success');
                return redirect('/specificjobrequests/' . $jobid);
            }
            else{
                Toastr::error('Something went wrong','Danger');
                return redirect('/specificjobrequests'.$jobid);

            }

        }catch (\Exception $e)
        {
            $res=$e->getMessage();
            Toastr::error($res,'Danger');
            return redirect('/specificjobrequests'.$jobid);

        }

    }
    /**
     * Give review to supplier who has completed the job and ask to review.
     *
     *
     */
    function giveSupplierReview(Request $request){
        if($request->star){
            $rating = $request->star;
        }else{
            $rating = 0;
        }
        $reviewContent = $request->reviewcontent;
        $jobId = $request->jobid;
        $supplierId = $request->supplierid;
        try {
            DB::beginTransaction();
            $data = [
                'user_id' => $supplierId,
                'request_id' => $jobId,
                'review' => $reviewContent,
                'rating' => $rating,
            ];

            SupplierReview::create($data);
            // get average rating of this supplier
            $result = DB::table('supplier_reviews')
                ->select(DB::raw('round(AVG(rating),0) as averagerating'))
                ->where('user_id',$supplierId)
                ->groupBy('user_id')
                ->first();
            // Change rating in supplier establishment table
            DB::table('supplier_establishments')
                ->where('user_id', $supplierId)
                ->update(array('rating' => $result->averagerating));  // update the record in the DB.
            // change status of job
            DB::table('supplier_requests')
                ->where(['user_id' => $supplierId, 'request_id' => $jobId])
                ->update(array('status' => 4 , 'is_reviewed' => 1));
            DB::commit();
            Toastr::Success('Review Sent. Job Completed','Success');
            return redirect('/specificjobrequests/'.$jobId);
        }catch (\Exception $e)
        {
            DB::rollback();
            $res=$e->getMessage();
            Toastr::error($res,'Danger');
            return redirect('/viewsupplierprofile/'.$supplierId.'/'.$jobId);

        }

    }
    /**
     * Display list of alljobs that supplier has accepted.
     *
     *
     */
    public function supplierinbox()
    {
        $supplierId= Auth::user()->id;
        $supplierNewRequest = DB::table('supplier_requests as a')
                              ->join('customer_requests as b','a.request_id','=','b.id')
                              ->select('b.description as jobdescription','b.created_at as created_date',
                              'b.id as jobid')
                             ->where('a.user_id',$supplierId)
                             ->where('a.status',0)
                             ->get();
        $supplierAcceptedRequest = DB::table('supplier_requests as a')
            ->join('customer_requests as b','a.request_id','=','b.id')
            ->select('b.description as jobdescription','b.created_at as created_date',
                'b.id as jobid','a.status as jobstatus')
            ->where('a.user_id',$supplierId)
            ->where('a.status','>',0)
            ->get();
        return view ('service.supplier.supplierinbox',compact('supplierNewRequest', 'supplierAcceptedRequest'));

    }
    /**
     * Display specific job detail for supplier alongwith buttons for furthur action.
     *
     *
     */
    public function myjobdetail($jobid)
    {

        $supplierId = Auth::user()->id;
        $jobDetail = DB::table('customer_requests as a')
                     ->join('categories as b','a.cat_id','=','b.id')
                     ->select('a.id as jobid','b.name as category','a.budget as jobbudget',
                     'a.timeframe as jobtime','a.area as jobaddress','a.description as jobdescription')
                     ->where('a.id',$jobid)
                     ->first();
        $checkRequestStatus = DB::table('supplier_requests')
                              ->where(['request_id'=>$jobid,'user_id'=>$supplierId])
                              ->first();
        $checkSupplierReviewd =  DB::table('supplier_reviews')
            ->select('rating','review')
            ->where(['request_id'=>$jobid,'user_id'=>$supplierId])
            ->count();
        if($checkSupplierReviewd >0){
            $customerReview = DB::table('supplier_reviews')
                ->select('rating','review')
                ->where(['request_id'=>$jobid,'user_id'=>$supplierId])
                ->first();

        }
        else{
            $customerReview = 0;

        }



        return view ('service.supplier.myjobdetail' , compact('jobDetail', 'checkRequestStatus' ,'customerReview'));

    }
    /**
     * Supplier accept customer job.
     *
     *
     */
    function acceptthisjob($jobid){
       $supplierId = Auth::user()->id;
       // check supplier credits
        $checkCredits = DB::table('users')->select('noofcredits')->where('id',$supplierId)->first();
        if($checkCredits->noofcredits == 0){
            Toastr::error('Insufficient credits. Please Buy More Credits','Danger');
            return redirect('/supplierinbox');

        }
       $check = DB::table('supplier_requests')
                ->where(['request_id'=>$jobid,'user_id'=>$supplierId,'status'=>0])
                ->count();
       if($check>0){
           DB::table('supplier_requests')
               ->where(['request_id'=>$jobid,'user_id'=>$supplierId,'status'=>0])
               ->update([
                   'status' => 1,
                   'accept_dateandtime' => date('Y-m-d H:i')
               ]);
           // reduce 1 credit
           $detail = DB::table('users')->select('noofcredits')->where('id',$supplierId)->first();
           $currentCredits = $detail->noofcredits;
           $newCredits = ($currentCredits)-(1);
           // update supplier credits
           $affected = DB::table('users')
               ->where(['id'=>$supplierId])
               ->update(['noofcredits' => $newCredits]);
           if($affected){
               Toastr::Success('Job Accepted.','Success');
               return redirect('/supplierinbox');
           }
       }
       else{
           $res= 'Something Went Wrong';
           Toastr::error($res,'Danger');
           return redirect('/supplierinbox');

       }

    }
    /**
     * Supplier completed the job and request to review.
     *
     *
     */
    function permissiontoreviewthisjob($jobid){
        $supplierId = Auth::user()->id;
        $check = DB::table('supplier_requests')
            ->where(['request_id'=>$jobid,'user_id'=>$supplierId,'status'=>2])
            ->count();
        if($check>0){
            DB::table('supplier_requests')
                ->where(['request_id'=>$jobid,'user_id'=>$supplierId,'status'=>2])
                ->update([
                    'status' => 3,
                ]);

            Toastr::Success('Job Completed.Review Request Sent','Success');
            return redirect('/supplierinbox');

        }
        else{
            $res= 'Something Went Wrong';
            Toastr::error($res,'Danger');
            return redirect('/supplierinbox');

        }


    }
    /**
     * Display all credit packages for suppliers to buy.
     *
     *
     */
    public function buycredits()
    {
        $creditsDetail = DB::table('category_packages')->where('status',true)->get();
        $counter = 1;
        return view ('service.supplier.buycredits' , compact('creditsDetail', 'counter'));

    }
    /**
     * Display form where supplier can add establishment details and images
     *
     *
     */
    public function supplierestablishments()
    {
        $userId = Auth::user()->id;
        $detail = DB::table('supplier_establishments')->where('user_id',$userId)->first();
        $categories = DB::table('categories as a')
            ->select ('a.id as cat_id','a.name as cat_name')
            ->where('a.status', true)
            ->whereNotExists(function ($query) {
                $query->select(DB::raw("b.id as subid"))
                    ->from('categories as b')
                    ->whereColumn('b.category_id', 'a.id');
            })
            ->get();
        $region = Region::select('*')
                          ->where('id','>',1)
                          ->get();

         if($detail){
             $establishmentImages = DB::table('supplier_establishment_images')->where('establishment_id', $detail->id)->get();
             $counter =1;
             return view ('service.supplier.editestablishment', compact('categories' ,'region', 'detail' ,'establishmentImages', 'counter'));
         }

        else{
            return view ('service.supplier.addestablishment', compact('categories' ,'region'));
        }

    }
    /**
     * save establishment basic detail and redirect user to add establishment image
     *
     *
     */
    public function savesupplierestablishment(AddEstablishmentRequest $request)
    {
       // dd($request);

        // insert record
        try{

            $geocode=file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng='.$request->latitude.','.$request->longitude.'&sensor=false&key=AIzaSyDE19uW2_YIeuZRC3IYFl1cMZ4QYSrqkbE&');
            $output= json_decode($geocode);
            $city = $output->results[0]->address_components[3]->long_name;


            /*for($j=0;$j<count($output->results[0]->address_components);$j++){
                echo '<b>'.$output->results[0]->address_components[$j]->types[0].': </b>  '.$output->results[0]->address_components[$j]->long_name.'<br/>';
            }*/
            if($city)
                DB::beginTransaction();
            $data = [
                'user_id' => $request->user_id,
                'cat_id' =>  $request->cat_id,
                'region_id' => $request->region_id,
                'name' => $request->name,
                'email' =>$request->email,
                'contactnumber' => $request->contactnumber,
                'orglatlng' => '('.$request->latitude.','.$request->longitude.')',
                'address' => $request->address,
                'suppliercity' =>$city,
                'latitude' => $request->latitude,
                'longitude' => $request->longitude,
                'about' => $request->about
            ];

            SupplierEstablishment::create($data);
            DB::commit();
            Toastr::Success('Establishment added.','Success');
            return redirect('/establishmentimages');

        }catch (\Exception $e)
        {
            $res=$e->getMessage();
            DB::rollback();
            Toastr::error($res,'Danger');
            return redirect('/supplierestablishments');
        }


    }
    /**
     * update supplier detail
     *
     *
     */
    public function updatesupplierestablishment(UpdateSupplierEstablishmentRequest $request){
        try {
            $recordid = $request->recordid;
            // get city from added address
            $geocode = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?latlng=' . $request->latitude . ',' . $request->longitude . '&sensor=false&key=AIzaSyDE19uW2_YIeuZRC3IYFl1cMZ4QYSrqkbE&');
            $output = json_decode($geocode);
            $city = $output->results[0]->address_components[3]->long_name;
            if ($city) {
                DB::beginTransaction();
                $updateEstablishment = SupplierEstablishment::find($recordid);
                $updateEstablishment->name = $request->name;
                $updateEstablishment->email = $request->email;
                $updateEstablishment->contactnumber = $request->contactnumber;
                $updateEstablishment->orglatlng = '(' . $request->latitude . ',' . $request->longitude . ')';
                $updateEstablishment->cat_id = $request->cat_id;
                $updateEstablishment->region_id = $request->region_id;
                $updateEstablishment->address = $request->address;
                $updateEstablishment->latitude = $request->latitude;
                $updateEstablishment->longitude = $request->longitude;
                $updateEstablishment->about = $request->about;
                $updateEstablishment->suppliercity = $city;
                $updateEstablishment->save();

                DB::commit();
                Toastr::Success('Establishment updated.','Success');
                return redirect('/supplierestablishments');
            }
        }catch (\Exception $e)
        {
            $res=$e->getMessage();
            DB::rollback();
            Toastr::error($res,'Danger');
            return redirect('/supplierestablishments');
        }



    }
    /**
     * Display dropzone for supplier to add images
     *
     *
     */
    public function establishmentimages()
    {
        $supplierId = Auth::user()->id;
        $detail = DB::table('supplier_establishments')->where('user_id',$supplierId)->first();
        return view ('service.supplier.addsupplierimages',compact('detail'));

    }
    /**
     * Save images that are added in dropzone
     *
     *
     */
    public function saveestablishmentimages(Request $request){
        $establishmentId = $request->establishment_id;
        $userId = Auth::user()->id;
        $image = $request->file('file');
        $extension = $image->extension();
        $path= $image->store('public/establishmentimages');
        $data = [
            'establishment_id' => $establishmentId,
            'user_id' => $userId,
            'image'   => $path
        ];
        SupplierEstablishmentImages::create($data);
        return response()->json(['success'=>$path]);


    }
    /**
     * Delete Establishment Image
     *
     *
     */
    public function deleteestablishmentimage($id)
    {

       $establishmentImage = SupplierEstablishmentImages::find($id);
       if(!$establishmentImage){
           Toastr::error('Image cannot be deleted','Alert');
           return redirect('/supplierestablishments');

       }
       else{
           $establishmentImage = SupplierEstablishmentImages::find($id);
           Storage::delete($establishmentImage->image); //delete image from storage
           DB::table('supplier_establishment_images')->where('id', '=', $id)->delete();
           Toastr::error('Establishment Image is deleted','Alert');
           return redirect('/supplierestablishments');

       }
    }
    /**
     *Add Credits for supplier so he/she can accept the job
     *
     *
     */

    function addcredits($id){
        $supplierId = Auth::user()->id;
       $detail = DB::table('users')->where('id',$supplierId)->first();
       $previousCredits = $detail->noofcredits;
       $newCredits = ($previousCredits)+$id;
        $affected = DB::table('users')
            ->where(['id'=>$supplierId])
            ->update(['noofcredits' => $newCredits]);
       if($affected){
           Toastr::Success(' '.$id.' Credit(s) added','Success');
           return redirect('/home');

       }
    }

}
