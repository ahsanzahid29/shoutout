<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\CreditsPackage;
use Illuminate\Http\Request;
use App\Http\Requests\PaymentSettingsRequest;
use App\Http\Requests\CreditsPackageRequest;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\Models\Cms;
use DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Brian2694\Toastr\Facades\Toastr;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //---- to add role-----
       // Role::create(['name' => 'supplier']);
        //---- to add permission-----
        //Permission::create(['name' => 'first permission']);
        #------------- Assign Role to permission----------------------#
        /*  $role= Role::findById(2);
        $permission=Permission::findById(8);
        $role->givePermissionTo($permission); */
        #--------------- Remove role for a permission
        /*$role=Role::findById(2);
        $permission=Permission::findById(8);
        $permission->removeRole($role);*/
        #--------------- Remove permission for a role
        /* $role=Role::findById(2);
        $permission=Permission::findById(8);
        $role->revokePermissionTo($permission); */
     /*----------------- You can assign permission directly to loggedin user
        This will go in table in model_has_permissions-----------------*/
        //auth()->user()->givePermissionTo('edit credit Package');
        /*---------------- we can assign role to loggedin user.
        This will add in model_has_roles-----------------------*/
       //auth()->user()->assignRole('customer');
        #---------------- To see user permissions directly and via role-----------------------#
        //dd(auth()->user()->getAllPermissions());
        #--------------- To get user count for specific role-----------------------#
       // dd(User::role('admin')->get());
       #--------------- To revoke permission for specific user-----------------------#
      // auth()->user()->revokePermissionTo('edit credit Package');
      #----------------- To revoke role for specifc user------------------------#
      //return auth()->user()->removeRole('admin');
      #---------- to check user has specific role---------------------#
      /*if(auth()->user()->hasRole('customer'))
{
    dd('My role is customer');
}*/
    //auth()->user()->assignRole('admin');

    return view('home');
    }
   /**
     * Display Payment Settings and list of credit packages.
     *
     */

    public function paymentsettings()
    {
        $cms = Cms::find(Auth::user()->id);
        $creditsPackages = DB::table('category_packages')
            ->select('*')
          ->get();
        return view ('settings.paymentsettings',compact('cms', 'creditsPackages'));

    }
    /**
     * Update payment settings of admin.
     *
     */
    public function updatepaymentsettingssection(PaymentSettingsRequest $request)
    {
        $cms = Cms::find(Auth::user()->id);
        $cms->merchantid = $request->merchantid;
        $cms->merchantkey = $request->merchantkey;
        $cms->save();
        Toastr::success('Payment settings updated successfully','Success');
        return redirect('/paymentsettings');
    }
    /**
     * Update Credit Package Information.
     *
     */

    function updatecreditpackagessection(Request $request)
    {
        $affected = DB::table('category_packages')
            ->where(['id'=>$request->recordid])
            ->update(['creditprice' => $request->creditprice]);
        Toastr::success('Credit Price changed successfully', 'Success');
        return redirect('/paymentsettings');

    }
    /**
     * Display Profile Settings of loggedin user.
     *
     */
    public function profilesettings()
    {
        $user = User::find(Auth::user()->id);
        return view ('settings.profilesettings', compact('user'));

    }
    // Update user profile
    function updateprofilesettings(Request $request){
        $this->validate(
            $request,
            [
                'name'             => 'required',
                'profileimage'     => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
                'password'         => 'nullable|min:6'
            ],
            [
                'name.required'      =>   'Please fill your name',
                'password.min'       =>    'Password must be 6 digits long',
                //'uEmail.unique'      => 'Sorry, This Email Address Is Already Used By Another User. Please Try With Different One, Thank You.',
                'profileimage.image' =>  'Please upload image only',
                'profileimage.mimes' => 'Please upload jpeg,png,jpg,gif,svg images .',
                'profileimage.max'   =>  'Image should be less than 3MB'
            ]
        );
        try{
            DB::beginTransaction();
            if($request->hasFile('profileimage'))
            {
                $path = $request->file('profileimage')->store('public/userimages');
            }
            $user = User::find(Auth::user()->id);
            $user->name = $request->name;
            if($request->hasFile('profileimage')){
                $user->profileimage = $path;
            }
            if($request->password){
                $user->password = Hash::make($request->password);
                $user->org_password = Crypt::encryptString($request->password);
            }
            $user->save();
            DB::commit();
            Toastr::success('Profile updated successfully', 'Success');
            return redirect('/profilesettings');
        }
        catch(\Exception $e){
            DB::rollBack();
            Toastr::error($e->getMessage(), 'Error');
            return redirect('/profilesettings');

        }
    }
}
