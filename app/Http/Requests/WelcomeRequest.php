<?php

namespace App\Http\Requests;

use App\Rules\VimeoLink;
use Illuminate\Foundation\Http\FormRequest;

class WelcomeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'extravideo' => ['required','url', new VimeoLink],
            //
        ];
    }
    public function messages()
    {
        return [
            'extravideo.required'         => 'Video Link is required',
            'extravideo.url'              => 'Please provide a valid URL',
        ];
    }
}
