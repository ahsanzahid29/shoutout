<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategorySaveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:categories',
            'categoryimage' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'status'        => 'required'
        ];
    }
    public function messages()
    {
        return [
            'name.required'         => 'Category Name is required',
            'name.unique'           => 'Category name must be unique',
            'categoryimage.required' => 'Please upload category image',
            'categoryimage.mimes'   => 'Only jpeg,png,jpg,gif,svg formats are supported',
            'categoryimage.max'     =>    'Image size should be less than 3 MB',
            'categoryimage.image'   =>   'Upload Image only',
            'status'                => 'Please Select Status'

        ];
    }
}
