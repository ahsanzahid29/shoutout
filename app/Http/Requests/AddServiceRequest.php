<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddServiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'cat_id' => 'required|not_in:0',
            'budget' => 'required',
            'timeframe' => 'required',
            'area' => 'required',
            'description' => 'required'

        ];
    }
    public function messages()
    {
        return[
            'cat_id.required'        =>   'Please Select Category',
            'cat_id.not_in'           =>   'Please Select Category',
            'budget.required'         =>   'Please provide job budget',
            'timeframe.required'      =>   'Please provide timeframe',
            'area.required'            =>  'Please provide area',
            'description.required'        => 'Please provide job description',

        ];
    }
}
