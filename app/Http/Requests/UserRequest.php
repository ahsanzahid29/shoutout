<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required',
            'email' => 'required|unique:users',
            'role_id' => 'required|not_in:0',
            'status' => 'required'
        ];
    }
    public function messages()
    {
        return [
            'name.required' => 'Name of User is required',
            'email.required' => 'Please give an email address to user',
            'email.unique' => 'Email address must be unique',
            'role_id.required' => 'Please Select Role',
            'role_id.not_in' =>'Please Select Role',
            'status.required' => 'Status is required'
        ];
    }
}
