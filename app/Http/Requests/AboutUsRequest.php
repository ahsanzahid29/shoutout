<?php

namespace App\Http\Requests;

use App\Rules\VimeoLink;
use Illuminate\Foundation\Http\FormRequest;

class AboutUsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'aboutusvideo' => ['required','url', new VimeoLink],
            'aboutusdescription' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'aboutusvideo.required'         => 'About Us Video Link is required',
            'aboutusvideo.url'              => 'Please provide a valid URL',
            'aboutusdescription.required'   => 'About Us Description is required',
        ];
    }
}
