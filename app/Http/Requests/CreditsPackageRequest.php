<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreditsPackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
              'creditprice' => 'required|integer|digits_between:0,99999',
            //
        ];
    }
    public function messages()
    {
        return [
            'creditprice.required'         => 'Price is required',
            'creditprice.integer'          => 'Only numbers are allowed',
            'creditprice.digits_between'   => 'Please enter valid price',
        ];
    }
}
