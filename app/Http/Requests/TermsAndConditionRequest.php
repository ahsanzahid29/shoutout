<?php

namespace App\Http\Requests;

use App\Rules\VimeoLink;
use Illuminate\Foundation\Http\FormRequest;

class TermsAndConditionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'termsvideo' => ['required','url', new VimeoLink],
            'termsdescription' => 'required',
        ];
    }
    public function messages()
    {
        return [
            'termsvideo.required'         => 'Video Link is required',
            'termsvideo.url'              => 'Please provide a valid URL',
            'termsdescription.required'   => 'Description is required',
        ];
    }
}
