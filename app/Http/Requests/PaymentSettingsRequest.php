<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentSettingsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'merchantid' => 'required|integer|digits_between:0,13',
            'merchantkey' =>'required|size:15'
            //
        ];
    }
    public function messages()
    {
        return [
            'merchantid.required'         => 'Merchant Id is required',
            'merchantid.integer'          => 'Only numbers are allowed',
            'merchantid.digits_between'   => 'Only 13 characters allowed',
            'merchantkey.required'        => 'Merchant Key is required',
            'merchantkey.size'             => 'Only 15 characters allowed'

        ];
    }
}
