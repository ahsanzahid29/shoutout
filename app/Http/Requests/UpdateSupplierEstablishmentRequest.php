<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateSupplierEstablishmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'user_id' => 'required',
            'name' => 'required',
            'email' => 'required',
            'contactnumber' => 'required',
            'cat_id' => 'required|not_in:0',
            'region_id' => 'required| not_in:0',
            'address' => 'required',
            'about' => 'required'

        ];
    }
    public function messages()
    {
        return[
            'user_id.required'       => 'User ID is required',
            'name.required'          => 'Please provide your name',
            'email.required'         => 'Please provide your email',
            'contactnumber.required' => 'Please provide your contact number',
            'cat_id.required'        => 'Please Select Category',
            'cat_id.not_in'           => 'Please Select Category',
            'region_id.required'      => 'Please Select Region',
            'region_id.not_in'        => 'Please Select Region',
            'address.required'        => 'Please provide your address',
            'about.required'          =>  'Please provide establishment about information'

        ];
    }
}
