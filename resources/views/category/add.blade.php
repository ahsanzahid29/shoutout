@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <form action="{{ route('category.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="InputCategoryName">Category Name</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="InputCategoryName" placeholder="Enter Category Name">
                @error('name')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="selectParentCategory">Parent Category</label>
                <select class="form-control" id="selectParentCategory" name="category_id">
                    <option value="0">No Parent</option>
                    @foreach($category as $row)
                    <option value="{{ $row->id }}">{{ $row->name }}</option>
                    @endforeach
                   </select>
            </div>
            <div class="form-group">
                <label for="InputCategoryImage">Category Image</label>
                <input type="file" name="categoryimage" class="form-control @error('categoryimage') is-invalid @enderror" id="InputCategoryImage">
                @error('categoryimage')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="InputCategoryImage">Status</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input @error('status') is-invalid @enderror" type="radio" name="status" id="publish" checked value="true">
                    <label class="form-check-label" for="publish">Publish</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input @error('status') is-invalid @enderror" type="radio" name="status" id="unPublish" value="false">
                    <label class="form-check-label" for="unPublish">Unpublish</label>
                </div>


            </div>
            <button type="submit" class="btn btn-primary">Add Category</button>
            <a href="{{url('/category')}}" class="btn btn-light">Cancel</a>

        </form>
    </div>

    @endsection
