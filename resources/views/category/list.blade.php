@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <div class="row justify-content-center">
            <div class="col-md-11">
            </div>
            <div class="col-md-1 mb-2">
                <a href="{{url('/category/create')}}" class="btn btn-primary" >Add Category</a>

            </div>
        </div>

    <table class="table" id="category">
        <thead>
        <tr>
            <th scope="col">Category</th>
            <th scope="col">Parent Category</th>
            <th scope="col">Status</th>
            <th scope="col">Action</th>
        </tr>
        </thead>
        <tbody>
        @foreach($category as $row)
        <tr>
            <th scope="row">{{$row->name}}</th>
            <td>{{isset($row->parentCategory->name) ? $row->parentCategory->name : "NA"}}</td>
            <td> @if($row->status=='true')
                    <span class="badge badge-success">Publish</span>
                @else
                    <span class="badge badge-danger">Un-publish</span>
                @endif
                </td>
            <td><a class="btn btn-sm btn-warning float-left" href="{{URL::to('category/'.$row->id.'/edit')}}">Edit</a>
                <form action="{{route('deleteestablishmentimage',$row->id)}}" method="post">
                 @csrf
                    <button type="submit" class="btn btn-sm btn-danger float-left ml-1" onclick="return confirm ('Are you sure to delete this category')">
                        Delete
                    </button>
                </form>
                </td>
        </tr>
            @endforeach
        </tbody>
    </table>
    </div>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#category').DataTable();
        } );
    </script>


@endsection
