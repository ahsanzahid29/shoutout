@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
<div class="col-md-3">
    <ul class="nav flex-column">
        @role('admin')
        <li class="nav-item">
            <a class="nav-link active" href="{{url('/home')}}">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('cms')}}">CMS</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{url('/category')}}">Category</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('userslist')}}">Users</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('profilesettings')}}">Settings</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('paymentsettings')}}">Payment Settings</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"  onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </li>
        @endrole
        @role('customer')
        <li class="nav-item">
            <a class="nav-link active" href="{{url('/home')}}">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('newservice')}}">Request Supplier</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('customerinbox')}}">Customer Inbox</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('profilesettings')}}">Settings</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"  onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </li>
        @endrole
        @role('supplier')
        <li class="nav-item">
            <a class="nav-link active" href="{{url('/home')}}">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('supplierestablishments')}}">My Establishment</a>
        </li>
        <li class="nav-item">
            <a class="nav-link " href="{{route('supplierinbox')}}">Supplier Inbox</a>
        </li>

        <li class="nav-item">
            <a class="nav-link" href="{{route('buycredits')}}">Buy Credits</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('profilesettings')}}">Settings</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}"  onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                @csrf
            </form>
        </li>
        @endrole

    </ul>
</div>
        @yield('content1')
    </div>
</div>
@endsection
