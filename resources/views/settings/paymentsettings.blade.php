@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <p class="h2">Change Payment Settings</p>
        <form action="{{route('updatepaymentsettings')}}" method="post">
            @csrf
            <label for="InputCategoryName">Mercahant ID</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">$</span>
                </div>
                <input type="text" name="merchantid" class="form-control @error('merchantid') is-invalid @enderror" placeholder="Merchant ID"  aria-describedby="basic-addon1" value="{{$cms['merchantid']}}">
                @error('merchantid')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <label for="InputCategoryName">Mercahant Key</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">$</span>
                </div>
                <input type="text" name="merchantkey" class="form-control @error('merchantkey') is-invalid @enderror" placeholder="Merchant Key"  aria-describedby="basic-addon1" value="{{$cms['merchantkey']}}">
                @error('merchantkey')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <button type="submit" class="btn btn-info">Update Payfast Settings</button>
        </form>
        <p class="h2 mt-3">Credit Packages</p>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Credits</th>
                <th scope="col">Price</th>
                </tr>
            </thead>
            <tbody>
            @foreach($creditsPackages as $row)
                <tr>
                    <th scope="row">{{ $row->creditname }}</th>
                    <td>
                        <form action="{{route('updatecreditpackages')}}" method="post" id="creditForm{{$row->id}}">
                            <input type="hidden" name="recordid" value="{{$row->id}}">
                            @csrf
                            <div class="form-group">
                        <div class="input-group mb-3 float-left" style="width:70%">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">ZAR</span>
                            </div>
                            <input type="text" class="form-control" name="creditprice" placeholder="Credit Price"  aria-describedby="basic-addon1" value="{{$row->creditprice}}" onkeypress="return isNumber(event)" required >
                        </div>
                            </div>
                            <input type="submit" style="width:20%" class="btn btn-info float-left ml-2" value="Update"/>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode!=46) {
                return false;
            }

            return true;
        }
        $(document).ready( function () {
            jQuery.validator.setDefaults({
                errorElement: 'span',
                errorPlacement: function (error, element) {
                    error.addClass('invalid-feedback');
                    element.closest('.form-group').append(error);
                },
                highlight: function (element, errorClass, validClass) {
                    $(element).addClass('is-invalid');
                },
                unhighlight: function (element, errorClass, validClass) {
                    $(element).removeClass('is-invalid');
                }
            });
            $('#creditpackages').DataTable();
            $("#creditForm1").submit(function(event){
                event.preventDefault();
                $("#creditForm1").validate({
                    // Specify validation rules
                    rules: {
                        creditprice1: {
                            required: true,
                            number: true
                        }
                    },
                    // Specify validation error messages
                    messages: {
                        creditprice1:{
                            required: "Please enter price",
                            number: "Only numbers are allowed"

                        }
                    },
                    // Make sure the form is submitted to the destination defined
                    // in the "action" attribute of the form when valid
                    submitHandler: function(form) {
                        form.submit();
                    }

                });
            });
        });

    </script>
@endsection
