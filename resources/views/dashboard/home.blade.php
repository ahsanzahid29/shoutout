@extends('layouts.sidebar')
@section('content1')
        <div class="col-md-9">
            <p class="h1">Dashboard</p>
            <div class="row">
                <div class="col-md-4">
                    <div class="card text-white bg-primary mb-3" >

                        <div class="card-body">
                            <h5 class="card-title">0</h5>
                            <p class="card-text">Total Users added</p>
                        </div>
                    </div>
                </div>
                    <div class="col-md-4">
                        <div class="card text-white bg-secondary mb-3" >

                            <div class="card-body">
                                <h5 class="card-title">0</h5>
                                <p class="card-text">Total Suppliers added</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card text-white bg-success mb-3" >
                            <div class="card-body">
                                <h5 class="card-title">0</h5>
                                <p class="card-text">Total Customers added</p>
                            </div>
                        </div>
                    </div>
                </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card text-white bg-primary mb-3" >

                        <div class="card-body">
                            <h5 class="card-title">0</h5>
                            <p class="card-text">Supplier added (Last 24 Hrs)</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card text-white bg-secondary mb-3" >

                        <div class="card-body">
                            <h5 class="card-title">0</h5>
                            <p class="card-text">Supplier added (Last 7 Days)</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card text-white bg-success mb-3" >
                        <div class="card-body">
                            <h5 class="card-title">0</h5>
                            <p class="card-text">Supplier added (Last 30 Days)</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card text-white bg-primary mb-3" >

                        <div class="card-body">
                            <h5 class="card-title">0</h5>
                            <p class="card-text">Customer added (Last 24 Hrs)</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card text-white bg-secondary mb-3" >

                        <div class="card-body">
                            <h5 class="card-title">0</h5>
                            <p class="card-text">Customer added (Last 7 Days)</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card text-white bg-success mb-3" >
                        <div class="card-body">
                            <h5 class="card-title">0</h5>
                            <p class="card-text"><span class="d-inline-block text-truncate" style="max-width: 250px;">Customer added (Last 30 Days)</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card text-white bg-primary mb-3" >

                        <div class="card-body">
                            <h5 class="card-title">0</h5>
                            <p class="card-text">Request added (Last 24 Hrs)</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card text-white bg-secondary mb-3" >

                        <div class="card-body">
                            <h5 class="card-title">0</h5>
                            <p class="card-text">Request added (Last 7 Days)</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card text-white bg-success mb-3" >
                        <div class="card-body">
                            <h5 class="card-title">0</h5>
                            <p class="card-text"><span class="d-inline-block text-truncate" style="max-width: 250px;">Request added (Last 30 Days)</span></p>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>

@endsection
