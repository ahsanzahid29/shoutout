@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-newRequest-tab" data-toggle="pill" href="#pills-newRequest" role="tab" aria-controls="pills-newRequest" aria-selected="true">New Request</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-awardedRequest-tab" data-toggle="pill" href="#pills-awardedRequest" role="tab" aria-controls="pills-awardedRequest" aria-selected="false">Awarded Request</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-newRequest" role="tabpanel" aria-labelledby="pills-newRequest-tab">
                <table class="table" id="newRequest">
                    <thead>
                    <tr>
                        <th scope="col">Supplier Name</th>
                        <th scope="col">Rating</th>
                        <th scope="col">Detail</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($supplierDetail as $row)
                     <tr>
                            <th scope="row">{{ $row->suppliername }}</th>
                            <td>@php
                               if($row->supplierrating==0.0){
                               echo 'NA';

                               }
                                if($row->supplierrating<2.0 && $row->supplierrating>1.0 ){
                               echo '*';

                               }
                                if($row->supplierrating<3.0 && $row->supplierrating>2.0){
                               echo '**';

                               }
                                if($row->supplierrating<4.0 && $row->supplierrating>3.0){
                               echo '***';

                               }
                                 if($row->supplierrating<5.0 && $row->supplierrating>4.0){
                               echo '****';

                               }
                                  if($row->supplierrating>4.9){
                               echo '*****';

                               }
                                    @endphp
                            </td>
                            <td><a href="{{route('viewsupplierprofile', [$row->supplier_id, $jobid])}}">View</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="pills-awardedRequest" role="tabpanel" aria-labelledby="pills-awardedRequest-tab">
                <span class="mt-5"></span>
                <table class="table" id="awardedRequest">
                    <thead>
                    <tr>
                         <th scope="col">Supplier Name</th>
                         <th scope="col">Rating</th>
                         <th scope="col">Job status</th>
                         <th scope="col">Detail</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($awardSuplierDetail as $row1)
                        <tr>
                            <th scope="row">{{ $row1->suppliername }}</th>
                            <td>@php
                                    if($row1->supplierrating==0.0){
                                    echo 'NA';

                                    }
                                     if($row1->supplierrating<2.0 && $row1->supplierrating>1.0 ){
                                    echo '*';

                                    }
                                     if($row1->supplierrating<3.0 && $row1->supplierrating>2.0){
                                    echo '**';

                                    }
                                     if($row1->supplierrating<4.0 && $row1->supplierrating>3.0){
                                    echo '***';

                                    }
                                      if($row1->supplierrating<5.0 && $row1->supplierrating>4.0){
                                    echo '****';

                                    }
                                       if($row1->supplierrating>4.9){
                                    echo '*****';

                                    }
                                @endphp
                            </td>
                            <td>
                                @if($row1->supplierjobstatus==2)
                                    <span class="badge badge-success">Awarded</span>
                                    @elseif($row1->supplierjobstatus==3)
                                    <span class="badge badge-warning">Pending For Review</span>
                                @elseif($row1->supplierjobstatus==4)
                                    <span class="badge badge-info">Reviewed</span>

                                @endif
                            </td>
                            <td><a href="{{route('viewsupplierprofile', [$row1->supplier_id, $jobid])}}">View</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#newRequest,#awardedRequest').DataTable();
        } );
    </script>
@endsection
