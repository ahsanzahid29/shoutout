@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-8">
                <div class="card" style="width: 100%">
                <div class="card-body">
                    <h5 class="card-title">Supplier Information</h5>
                <div id="demo" class="carousel slide" data-ride="carousel">
                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        @for($i=0;$i<count($supplierImages);$i++)
                            <li data-target="#demo" data-slide-to="{{$i}}"
                                @php
                                    if($i==0){
                                        echo ' class="active" ';
                                    }
                                @endphp
                            >

                            </li>
                        @endfor
                    </ul>
                    <!-- The slideshow -->
                    <div class="carousel-inner">
                        @foreach($supplierImages as $row)
                            @if($counter==0)
                                <div class="carousel-item active">
                                    <img src="{{ Storage::url($row->supplierimage) }}" alt="Establishment Image" width="1100" height="300">
                                </div>
                            @else
                                <div class="carousel-item">
                                    <img src="{{ Storage::url($row->supplierimage) }}" alt="Establishment Image" width="1100" height="300">
                                </div>
                            @endif
                            @php
                                $counter++;
                            @endphp
                        @endforeach

                    </div>

                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#demo" data-slide="prev">
                        <span class="carousel-control-prev-icon"></span>
                    </a>
                    <a class="carousel-control-next" href="#demo" data-slide="next">
                        <span class="carousel-control-next-icon"></span>
                    </a>
                </div>
                    <div class="mt-1"></div>
                    <label for="InputCategoryName">Supplier Category</label>
                    <div class="input-group mb-3">
                        <input value="{{$supplierEstdDetail->category}}" disabled type="text" class="form-control" placeholder="Supplier category"  aria-describedby="basic-addon1">
                    </div>
                    <label for="InputCategoryName">Supplier Region</label>
                    <div class="input-group mb-3">
                        <input value="{{$supplierEstdDetail->region}}" disabled type="text" class="form-control" placeholder="Supplier region"  aria-describedby="basic-addon1">
                    </div>
                    <label for="InputCategoryName">Supplier Name</label>
                    <div class="input-group mb-3">
                        <input value="{{$supplierEstdDetail->name}}" disabled type="text" class="form-control" placeholder="Supplier region"  aria-describedby="basic-addon1">
                    </div>
                    <label for="InputCategoryName">Supplier Email</label>
                    <div class="input-group mb-3">
                        <input value="{{$supplierEstdDetail->email}}" disabled type="text" class="form-control" placeholder="Supplier region"  aria-describedby="basic-addon1">
                    </div>
                    <label for="InputCategoryName">Supplier Phone</label>
                    <div class="input-group mb-3">
                        <input value="{{$supplierEstdDetail->contactnumber}}" disabled type="text" class="form-control" placeholder="Supplier region"  aria-describedby="basic-addon1">
                    </div>
                    <label for="InputCategoryName">Supplier Address</label>
                    <div class="input-group mb-3">
                        <textarea disabled class="form-control" cols="5" rows="5">{{$supplierEstdDetail->address}}</textarea>
                    </div>
                    <label for="InputCategoryName">About Supplier</label>
                    <div class="input-group mb-3">
                        <textarea disabled class="form-control" cols="5" rows="5">{{$supplierEstdDetail->about}}</textarea>
                    </div>
                    @if($jobStatus->status==1)
                    <a href="{{route('awardjob',[$jobDetail->id,$supplierEstdDetail->user_id])}}" class="btn btn-success">Award Job</a>
                     @elseif($jobStatus->status==2)
                        <a href="javascript:void(0);" class="btn btn-info">Job Awarded</a>
                    @elseif($jobStatus->status==3)
                        <a data-toggle="modal" data-target="#giveReviewModal" href="javascript:void(0);" class="btn btn-warning">Give Review</a>
                    @elseif($jobStatus->status==4)
                        <a href="javascript:void(0);" class="btn btn-primary">Job Completed</a>
                    @endif
                    <a href="{{route('specificjobrequests',$jobDetail->id)}}" class="btn btn-secondary">Go Back</a>
            </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card" style="width: 100%">
                    <div class="card-body">
                        <h5 class="card-title">Job Information</h5>
                        <div class="mt-1"></div>
                        <label for="InputCategoryName">Job Category</label>
                        <div class="input-group mb-3">
                            <input value="{{$jobDetail->cat_name}}" disabled type="text" class="form-control" placeholder="Job category"  aria-describedby="basic-addon1">
                        </div>
                        <label for="InputCategoryName">Budget</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text" id="basic-addon1">$</span>
                            </div>
                            <input disabled value="{{$jobDetail->budget}}" type="text" class="form-control" placeholder="Job Budget"  aria-describedby="basic-addon1">
                        </div>
                        <div class="form-group">
                            <label for="InputCategoryName">Timeframe</label>
                            <input disabled value="{{$jobDetail->timeframe}}" type="text" class="form-control" id="InputCategoryName" placeholder="Specify Job Timeframe">
                        </div>
                        <div class="form-group">
                            <label for="InputCategoryName">Job Area</label>
                            <textarea disabled class="form-control" rows="5" cols="5">{{$jobDetail->area}}</textarea>
                        </div>
                        <div class="form-group">
                            <label for="InputCategoryName">Job Description</label>
                            <textarea disabled class="form-control" rows="5" cols="5">{{$jobDetail->description}}</textarea>
                        </div>
                    </div>
                </div>


            </div>
        </div>

    </div>
    <!-- Modal -->
    <div class="modal fade" id="giveReviewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Give Review</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{route('givereview')}}" method="post">
                    <input type="hidden" name="jobid" value="{{$jobDetail->id}}" />
                    <input type="hidden" name="supplierid" value="{{$supplierEstdDetail->user_id}}" />
                    @csrf
                <div class="modal-body">
                            <div class="stars">
                                <input class="star star-5" id="star-5" type="radio" value="5" name="star" />
                                <label class="star star-5" for="star-5"></label>
                                <input class="star star-4" id="star-4" type="radio" value="4" name="star" />
                                <label class="star star-4" for="star-4"></label>
                                <input class="star star-3" id="star-3" type="radio" value="3" name="star" />
                                <label class="star star-3" for="star-3"></label>
                                <input class="star star-2" id="star-2" type="radio" value="2" name="star" />
                                <label class="star star-2" for="star-2"></label>
                                <input class="star star-1" id="star-1" type="radio" value="1" name="star" />
                                <label class="star star-1" for="star-1"></label>
                            </div>
                        <div class="form-group">
                            <textarea name="reviewcontent" class="form-control" rows="5" cols="5" placeholder="Write something about supplier.." id="message-review"></textarea>
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
