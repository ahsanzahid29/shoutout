@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <table class="table" id="customerinbox">
            <thead>
            <tr>
                <th style="display:none;"></th>
                <th scope="col">Job Title</th>
                <th scope="col">Created At</th>
                <th scope="col">Category</th>
                <th scope="col">Job status</th>
                <th scope="col">Suppliers</th>
                <th scope="col">Detail</th>
            </tr>
            </thead>
            <tbody>
            @foreach($detail as $row)
                @php
                $supplierCount = DB::table('supplier_requests')
                                ->where('request_id',$row->jobid)
                                ->count();
                @endphp
                <tr>
                    <td style="display:none"></td>
                    <th scope="row"><span data-toggle="tooltip" title="{{$row->jobdescription}}" class="d-inline-block text-truncate" style="max-width: 150px;">{{$row->jobdescription}}</span></th>
                    <td>{{date('d M,Y',strtotime($row->jobcreation))}}</td>
                    <td>{{$row->category}}</td>
                    <td>@if($row->jobstatus==1)<span class="badge badge-success">Open</span>@else<span class="badge badge-danger">closed</span>@endif</td>
                    <td>{{$supplierCount}}</td>
                    <td><a href="{{route('specificjobrequests', $row->jobid)}}">View Suppliers</a></td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        $(document).ready( function () {

            $('#customerinbox').DataTable();
            $('[data-toggle="tooltip"]').tooltip();
        } );
    </script>
@endsection
