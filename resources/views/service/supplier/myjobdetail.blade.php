@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <form>
            <div class="form-group">
                <label for="selectParentCategory">Category</label>
                <input type="text" class="form-control" value="{{$jobDetail->category}}"  aria-describedby="basic-addon1" disabled>
            </div>
            <label for="InputCategoryName">Budget</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">$</span>
                </div>
                <input type="text" class="form-control" value="{{$jobDetail->jobbudget}}"  aria-describedby="basic-addon1" disabled>
            </div>
            <div class="form-group">
                <label for="InputCategoryName">Timeframe</label>
                <input type="text" class="form-control" id="InputCategoryName" value="{{$jobDetail->jobtime}}" disabled>
            </div>
            <div class="form-group">
                <label for="InputCategoryName">Search Area/City</label>
                <input type="text" class="form-control" id="InputCategoryName" value="{{$jobDetail->jobaddress}}" disabled>
            </div>
            <div class="form-group">
                <label for="InputCategoryName">Job Description</label>
                <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" disabled>{{$jobDetail->jobdescription}}</textarea>
            </div>
           @if($checkRequestStatus->status==0)
               <a href="{{route('acceptjobDetail',$jobDetail->jobid)}}" class="btn btn-success">Accept Job Request</a>
               @elseif ($checkRequestStatus->status==1)
               <a href="javascript:void(0);" class="btn btn-info">Accepted</a>
            @elseif ($checkRequestStatus->status==2)
                <a href="{{route('reviewPendingjobDetail',$jobDetail->jobid)}}" class="btn btn-warning">Request For Review</a>
                @elseif ($checkRequestStatus->status==3)
                    <a href="javascript:void(0);" class="btn btn-info">Review Request Pending</a>
            @elseif ($checkRequestStatus->status==4)
                <a data-toggle="modal" data-target="#reviewModal" href="javascript:void(0);" class="btn btn-info">Review Completed</a>
                @else
               @endif
            <a href="{{route('supplierinbox')}}" class="btn btn-light">Go Back</a>
            </form>
    </div>
    <!-- Modal -->
    @if($customerReview)
    <div class="modal fade" id="reviewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Review</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="d-flex justify-content-center">
                        <div class="content text-center">
                            <div class="ratings">
                                <div class="stars">
                                    @for($i=0;$i<$customerReview->rating;$i++)
                                    <i class="fa fa-star"></i>
                                    @endfor
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="InputCategoryName">Review Content</label>
                                <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" disabled>{{$customerReview->review}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    @endif
@endsection
