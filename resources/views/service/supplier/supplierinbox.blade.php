@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-newRequest-tab" data-toggle="pill" href="#pills-newRequest" role="tab" aria-controls="pills-newRequest" aria-selected="true">New Requests</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-awardedRequest-tab" data-toggle="pill" href="#pills-awardedRequest" role="tab" aria-controls="pills-awardedRequest" aria-selected="false">Accepted Requests</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-newRequest" role="tabpanel" aria-labelledby="pills-newRequest-tab">
                <table class="table" id="newRequest">
                    <thead>
                    <tr>
                        <th scope="col">Job Description</th>
                        <th scope="col">Job created</th>
                        <th scope="col">Detail</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($supplierNewRequest as $rowNewRequest)
                        <tr>
                            <th scope="row"><span class="d-inline-block text-truncate" style="max-width: 150px;">{{ $rowNewRequest->jobdescription }}</span></th>
                            <td>{{ date ('d M, Y',strtotime($rowNewRequest->created_date)) }}</td>
                            <td><a target="_blank" href="{{route('myjobdetail',$rowNewRequest->jobid)}}">View</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
            <div class="tab-pane fade" id="pills-awardedRequest" role="tabpanel" aria-labelledby="pills-awardedRequest-tab">
                <span class="mt-5"></span>
                <table class="table" id="acceptedRequest">
                    <thead>
                    <tr>
                        <th scope="col">Job Description</th>
                        <th scope="col">Status</th>
                        <th scope="col">Detail</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($supplierAcceptedRequest as $rowAcceptedRequest)
                        <tr>
                            <th scope="row"><span class="d-inline-block text-truncate" style="max-width: 150px;">{{ $rowAcceptedRequest->jobdescription }}</span></th>
                            <td>@php
                                if($rowAcceptedRequest->jobstatus ==1){
                            echo '<span class="badge badge-info" style="color:white">Accepted</span>';
                            }
                            else if($rowAcceptedRequest->jobstatus ==2){
                            echo '<span class="badge badge-success" style="color:white">Awarded</span>';
                            }
                             else if($rowAcceptedRequest->jobstatus ==3){
                            echo '<span class="badge badge-warning" style="color:white">Review Pending</span>';
                            }
                                else if($rowAcceptedRequest->jobstatus ==4){
                            echo '<span class="badge badge-primary" style="color:white">Reviewed</span>';
                            }
                            else{
                                echo '<span class="badge badge-success">Other</span>';

                            }
                            @endphp</td>
                            <td><a target="_blank" href="{{route('myjobdetail',$rowAcceptedRequest->jobid)}}">View</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#newRequest,#acceptedRequest').DataTable();
        } );
    </script>
@endsection
