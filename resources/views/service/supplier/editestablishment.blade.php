@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <form action="{{route('updatesupplierestablishment')}}" method="post">
            @csrf
            <input type="hidden" name="recordid" value="{{$detail->id}}"/>
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}" />
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
      <input name="name" type="text" class="form-control @error('name') is-invalid @enderror" id="exampleInputEmail1"
             placeholder="Enter Name" value="{{$detail->name}}" >
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>


                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" id="exampleInputEmail1"
                               placeholder="Enter Email" value="{{$detail->email}}">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleContactNumber">Contact No</label>
                        <input name="contactnumber" type="text" class="form-control @error('contactnumber') is-invalid @enderror" id="exampleContactNumber"
                               placeholder="Enter Contact Number" value="{{$detail->contactnumber}}">
                        @error('contactnumber')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleEstablishmentCategory">Category</label>
                        <select name="cat_id" class="form-control @error('cat_id') is-invalid @enderror" id="exampleEstablishmentCategory">
                            <option value="0">Select Category</option>
                            @foreach($categories as $row)
                            <option value="{{$row->cat_id}}" @if($row->cat_id==$detail->cat_id) {{ 'selected' }}@endif>{{$row->cat_name}}</option>
                            @endforeach
                        </select>
                        @error('cat_id')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputProvince">Region</label>
                        <select name="region_id" class="form-control @error('region_id') is-invalid @enderror" id="exampleInputProvince">
                            <option value="0">Select Region</option>
                            @foreach($region as $regionrow)
                            <option value="{{$regionrow->id}}" @if($regionrow->id==$detail->region_id){{'selected'}} @endif>{{$regionrow->name}}</option>
                            @endforeach
                        </select>
                        @error('region_id')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Address</label>
                        <input  id="autocomplete" onFocus="geolocate()" name="address" type="text" class="form-control @error('address') is-invalid @enderror" id="exampleInputEmail1"
                                placeholder="Place Your address" value="{{$detail->address}}">
                        @error('address')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <input type="hidden" name="latitude" id="latitude" value="{{$detail->latitude}}">
                    <input type="hidden" name="longitude" id="longitude" value="{{$detail->longitude}}">
                    <input type="hidden" name="location_id" id="location_id" value="">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleEstablishmentAbout">About Establishment</label>
                        <textarea name="about" class="form-control @error('about') is-invalid @enderror" id="exampleEstablishmentAbout" rows="3">{{$detail->about}}</textarea>
                        @error('about')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-warning">Update Establishment</button>
          </form>
        <!-- Display establishment images in table -->
        @if(count($establishmentImages)>0)
            <a href="{{route('establishmentimages')}}" class="btn btn-success mt-2 mb-2 float-right">Add More Images</a>
        <table class="table mt-2">

            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Image</th>
                <th scope="col">Action</th>

            </tr>
            </thead>
            <tbody>
            @foreach($establishmentImages as $row)
            <tr>
                <th scope="row">{{$counter++}}</th>
                <td><img src ="{{ Storage::url($row->image) }}" alt="Establishment Image" width="50" height="50"/></td>
                <td>
                    <a href="{{route('deleteestablishmentimage' , $row->id)}}" onclick="return confirm('Are you sure you want to delete this image?');" class="btn btn-danger btn-sm">Delete</a>
                </td>
            </tr>
            @endforeach
            </tbody>
        </table>
            @endif
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDE19uW2_YIeuZRC3IYFl1cMZ4QYSrqkbE&&libraries=places&callback=initAutocomplete" async defer></script>
    <script type="text/javascript">
        function initAutocomplete() {
            var input = document.getElementById('autocomplete');
            var options = {}
           var autocomplete = new google.maps.places.Autocomplete(input, options);
           google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();
                var lat = place.geometry.location.lat();
                var lng = place.geometry.location.lng();
                var placeId = place.place_id;
                // to set city name, using the locality param
                var componentForm = {
                    locality: 'short_name',
                };
                $("#latitude").val(lat);
                $("#longitude").val(lng);
                $("#location_id").val(placeId);
            });
        }
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    var circle = new google.maps.Circle(
                        {center: geolocation, radius: position.coords.accuracy});

                });
            }

        }
    </script>
@endsection
