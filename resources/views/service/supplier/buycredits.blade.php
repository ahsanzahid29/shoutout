@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9 mt-md-5">
        <div class="row">
            @foreach($creditsDetail as $row)
            <div class="col-sm-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">{{$row->creditname}}</h5>
                        <p class="card-text"><p class="h6">Price: ZAR50</p></p>
                        @php
                        if($counter == 1){
                     echo '<p class="card-text"><p class="h6">No of credits:30</p></p>';
                     echo '<a href="'.route("addcredits","30").'" class="btn btn-primary">Buy Package</a>';
                        }
                        elseif($counter == 2){
                      echo '<p class="card-text"><p class="h6">No of credits:60</p></p>';
                       echo '<a href="'.route("addcredits","60").'" class="btn btn-primary">Buy Package</a>';

                        }
                        elseif($counter == 3){
                      echo '<p class="card-text"><p class="h6">No of credits:90</p></p>';
                       echo '<a href="'.route("addcredits","90").'" class="btn btn-primary">Buy Package</a>';
                        }
                        @endphp




                    </div>
                        @php
                        $counter++;
                        @endphp

                </div>
            </div>
            @endforeach

        </div>
    </div>
@endsection
