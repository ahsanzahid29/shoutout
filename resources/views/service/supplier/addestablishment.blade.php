@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <form action="{{route('addsupplierestablishment')}}" method="post">
            @csrf
            <input type="hidden" name="user_id" value="{{Auth::user()->id}}" />
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
      <input name="name" type="text" class="form-control @error('name') is-invalid @enderror" id="exampleInputEmail1"  placeholder="Enter Name" >
                        @error('name')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>


                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email</label>
                        <input name="email" type="email" class="form-control @error('email') is-invalid @enderror" id="exampleInputEmail1"  placeholder="Enter Email">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleContactNumber">Contact No</label>
                        <input name="contactnumber" type="text" class="form-control @error('contactnumber') is-invalid @enderror" id="exampleContactNumber"  placeholder="Enter Contact Number">
                        @error('contactnumber')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleEstablishmentCategory">Category</label>
                        <select name="cat_id" class="form-control @error('cat_id') is-invalid @enderror" id="exampleEstablishmentCategory">
                            <option value="0">Select Category</option>
                            @foreach($categories as $row)
                            <option value="{{$row->cat_id}}">{{$row->cat_name}}</option>
                            @endforeach
                        </select>
                        @error('cat_id')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputProvince">Region</label>
                        <select name="region_id" class="form-control @error('region_id') is-invalid @enderror" id="exampleInputProvince">
                            <option value="0">Select Region</option>
                            @foreach($region as $regionrow)
                            <option value="{{$regionrow->id}}">{{$regionrow->name}}</option>
                            @endforeach
                        </select>
                        @error('region_id')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Address</label>
                        <input  id="autocomplete" onFocus="geolocate()" name="address" type="text" class="form-control @error('address') is-invalid @enderror" id="exampleInputEmail1"  placeholder="Place Your address">
                        @error('address')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                    <input type="hidden" name="latitude" id="latitude" value="">
                    <input type="hidden" name="longitude" id="longitude" value="">
                    <input type="hidden" name="location_id" id="location_id" value="">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="exampleEstablishmentAbout">About Establishment</label>
                        <textarea name="about" class="form-control @error('about') is-invalid @enderror" id="exampleEstablishmentAbout" rows="3"></textarea>
                        @error('about')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>
                </div>
            </div>

            <button type="submit" class="btn btn-info">Add Establishment</button>
           <!-- <a href="{{route('establishmentimages')}}" class="btn btn-info">Add Establishment</a>-->
        </form>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDE19uW2_YIeuZRC3IYFl1cMZ4QYSrqkbE&&libraries=places&callback=initAutocomplete" async defer></script>
    <script type="text/javascript">
        function initAutocomplete() {
            var input = document.getElementById('autocomplete');
            var options = {}
           var autocomplete = new google.maps.places.Autocomplete(input, options);
           google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();
                var lat = place.geometry.location.lat();
                var lng = place.geometry.location.lng();
                var placeId = place.place_id;
                // to set city name, using the locality param
                var componentForm = {
                    locality: 'short_name',
                };
                $("#latitude").val(lat);
                $("#longitude").val(lng);
                $("#location_id").val(placeId);
            });
        }
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    var circle = new google.maps.Circle(
                        {center: geolocation, radius: position.coords.accuracy});

                });
            }

        }
    </script>
@endsection
