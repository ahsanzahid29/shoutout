@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <div class="card">
            <div class="card-header">
                Supplier Establishment Images
            </div>
            <div class="card-body">
                <form id="image-upload" enctype="multipart/form-data" class="dropzone dz-clickable" action="{{route('establshmentimages.store')}}">

                    @csrf
                    <input type="hidden" name="establishment_id" value="{{$detail->id}}"/>
                    <div>
                        <h3 class="text-center">Upload Image By click on box</h3>
                    </div>
                    <div class="dz-default dz-message"><span>Drop Files Here</span></div>
                </form>
            </div>
        </div>
    </div>
<script type="text/javascript">
    Dropzone.options.imageUpload = {
       acceptedFiles: ".jpeg,.jpg,.png,.gif"

    };
</script>
@endsection
