@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <form method="post" action="{{route('savejobrequest')}}">
            @csrf
            <div class="form-group">
                <label for="selectParentCategory">Select Category</label>
                <select class="form-control @error('cat_id') is-invalid @enderror" id="selectParentCategory" name="cat_id" >
                    <option value="0">Please Select Category</option>
                    @foreach($categories as $row)
                    <option value="{{$row->cat_id}}">{{$row->cat_name}}</option>
                    @endforeach

                </select>
                    @error('cat_id')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
            </div>
            <label for="InputCategoryName">Budget</label>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text" id="basic-addon1">$</span>
                </div>
                <input name="budget" type="text" class="form-control @error('budget') is-invalid @enderror" placeholder="Job Budget"  aria-describedby="basic-addon1">
                @error('budget')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="InputCategoryName">Timeframe</label>
                <input name="timeframe" type="text" class="form-control @error('timeframe') is-invalid @enderror" id="InputCategoryName" placeholder="Specify Job Timeframe">
                @error('timeframe')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="InputCategoryName">Search Area/City</label>
                <input name="area" onFocus="geolocate()" type="text" class="form-control @error('area') is-invalid @enderror" id="autocomplete" placeholder="Search area/city">
                @error('area')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <input type="hidden" name="latitude" id="latitude" value="">
            <input type="hidden" name="longitude" id="longitude" value="">
            <input type="hidden" name="location_id" id="location_id" value="">
            <div class="form-group">
                <label for="InputCategoryName">Job Description</label>
                <textarea name="description" class="form-control @error('description') is-invalid @enderror" id="exampleFormControlTextarea1" rows="3"></textarea>
                @error('description')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Send Request</button>
        </form>
    </div>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDE19uW2_YIeuZRC3IYFl1cMZ4QYSrqkbE&&libraries=places&callback=initAutocomplete1" async defer></script>
    <script type="text/javascript">
        function initAutocomplete1() {
            var input = document.getElementById('autocomplete');
            var options = {}
            var autocomplete = new google.maps.places.Autocomplete(input, options);
            google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();
                var lat = place.geometry.location.lat();
                var lng = place.geometry.location.lng();
                var placeId = place.place_id;
                // to set city name, using the locality param
                var componentForm = {
                    locality: 'short_name',
                };
                $("#latitude").val(lat);
                $("#longitude").val(lng);
                $("#location_id").val(placeId);
            });
        }
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    var circle = new google.maps.Circle(
                        {center: geolocation, radius: position.coords.accuracy});

                });
            }

        }
    </script>
@endsection
