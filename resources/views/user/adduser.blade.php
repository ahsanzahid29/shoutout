@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <form action="{{ route('saveuser') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="InputCategoryName">Full Name</label>
                <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" id="InputCategoryName" placeholder="Enter Full Name">
                @error('name')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="InputCategoryName">Email</label>
                <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" id="InputCategoryName" placeholder="Enter Email address">
                @error('email')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="selectUserRole">Select Role</label>
                <select class="form-control @error('role_id') is-invalid @enderror" id="selectUserRole" name="role_id">
                    <option value="0">Select Role</option>
                   @foreach($role as $row)
                        <option value="{{ $row->id }}">{{ $row->name }}</option>
                    @endforeach
                </select>
                @error('role_id')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="InputUserStatus">Status</label>
                <div class="form-check form-check-inline">
                    <input class="form-check-input @error('status') is-invalid @enderror" type="radio" name="status" id="publish" checked value="true">
                    <label class="form-check-label" for="publish">Active</label>
                </div>
                <div class="form-check form-check-inline">
                    <input class="form-check-input @error('status') is-invalid @enderror" type="radio" name="status" id="unPublish" value="false">
                    <label class="form-check-label" for="unPublish">Inactive</label>
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Add User</button>
            <a href="{{url('/userslist')}}" class="btn btn-light">Cancel</a>

        </form>
    </div>


@endsection
