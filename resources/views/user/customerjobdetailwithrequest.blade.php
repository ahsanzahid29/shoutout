@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-4">
                <div class="card border-warning mb-3" style="max-width: 18rem;">
                    <div class="card-header bg-transparent border-warning">Job Details</div>
                    <div class="card-body text-warning">
                        <form class="mt-2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Category</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Category" value="{{$detail->category}}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Customer Name</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Customer Name" value-="{{$detail->customername}}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Email" value="{{$detail->customeremail}}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Budget</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" value="{{$detail->budget}}"  placeholder="Budget" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Area</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" readonly>{{$detail->area}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">City</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" value="{{$detail->city}}"  placeholder="City" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Job Description</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" readonly>{{$detail->description}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card border-info mb-3" style="max-width: 18rem;">
                    <div class="card-header bg-transparent border-info">New Requests</div>
                    <div class="card-body text-info">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Supplier</th>
                                <th scope="col">Rating</th>
                                <th scope="col">Detail</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($newRequests as $rownew)
                            <tr>
                                <td>{{$rownew->suppliername}}</td>
                                <td>@php
                                    if($rownew->supplierrating== 0){
                                   echo 'NA';
                                   }
                                    elseif($rownew->supplierrating==5 ){
                                   echo '*****';
                                   }
                                   elseif($rownew->supplierrating> 0 || $rownew->supplierrating< 2){
                                echo '*' ;
                                   }
                               elseif($rownew->supplierrating> 2|| $rownew->supplierrating< 3){
                                echo '**' ;
                                   }
                                elseif($rownew->supplierrating> 3|| $rownew->supplierrating< 4){
                                echo '***' ;
                                   }
                                   elseif($rownew->supplierrating> 4|| $rownew->supplierrating< 5){
                                echo '****' ;
                                   }

                                @endphp
                                <td><a href="{{route('userdetail', $rownew->supplierid)}}" target="_blank">View</a></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card border-success mb-3" style="max-width: 18rem;">
                    <div class="card-header bg-transparent border-success">Awarded Requests</div>
                    <div class="card-body text-success">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Supplier</th>
                                <th scope="col">Rating</th>
                                <th scope="col">Detail</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($awardRequests as $rowaward)
                                <tr>
                                    <td>{{$rowaward->suppliername}}</td>
                                    <td>@php
                                            if($rowaward->supplierrating== 0){
                                           echo 'NA';
                                           }
                                            elseif($rowaward->supplierrating==5 ){
                                           echo '*****';
                                           }
                                           elseif($rowaward->supplierrating> 0 || $rowaward->supplierrating< 2){
                                        echo '*' ;
                                           }
                                       elseif($rowaward->supplierrating> 2|| $rowaward->supplierrating< 3){
                                        echo '**' ;
                                           }
                                        elseif($rowaward->supplierrating> 3|| $rowaward->supplierrating< 4){
                                        echo '***' ;
                                           }
                                           elseif($rowaward->supplierrating> 4|| $rowaward->supplierrating< 5){
                                        echo '****' ;
                                           }

                                        @endphp

                                    </td>
                                    <td><a href="{{route('userdetail', $rowaward->supplierid)}}" target="_blank">View</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Model Div  -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Supplier Detail</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    ...
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>
@endsection
