@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <div class="row justify-content-center">
            <div class="col-md-11">
            </div>
            <div class="col-md-1 mb-2">
                <a href="{{url('/adduser')}}" class="btn btn-success" >Add User</a>

            </div>
        </div>
        <form action="{{route('userslist')}}" method="GET">
        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="selectUserType">User Type</label>
                    <select class="form-control" id="selectUserType" name="usertype">
                        <option @if(request()->usertype==0) selected @endif value="0">All</option>
                        <option @if(request()->usertype==2) selected @endif value="2">Customer</option>
                        <option @if(request()->usertype==3) selected @endif value="3">Supplier</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="searchByName">Search By Name</label>
                    <input value="{{request()->username}}" type="text" class="form-control" id="searchByName" name="username" placeholder="Search..">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label for="selectStatus">Status</label>
                    <select class="form-control" id="selectStatus" name="userstatus">
                        <option @if(request()->userstatus==0) selected @endif value="0" >All</option>
                        <option @if(request()->userstatus==1) selected @endif value="1">Active</option>
                        <option @if(request()->userstatus==2) selected @endif value="2">InActive</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2">
                <button type="submit" class=" form-control btn btn-primary mt-4 ml-2">Search</button>
            </div>
        </div>
        </form>

        <table class="table" id="users">
            <thead>
            <tr>
                <th scope="col">User</th>
                <th scope="col">Created At</th>
                <th scope="col">Category</th>
                <th scope="col">Status</th>
                <th scope="col">Credits</th>
                <th scope="col">Action</th>
            </tr>
            </thead>
            <tbody>
            @foreach($user as $row)
                <tr>
                    <th scope="row">{{$row->name}}</th>
                    <td>{{ date('d M Y',strtotime($row->created_at))}}</td>
                    <td>---</td>
                    <td>{{$row->status==true ? 'active' : 'inactive'}}</td>
                    <td>{{ $row->noofcredits }}</td>
                    <td><a href="{{route('userdetail' , $row->id)}}">View</a></td>
                </tr>
            @endforeach

            </tbody>
        </table>
    </div>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#users').DataTable();
        } );
    </script>
@endsection
