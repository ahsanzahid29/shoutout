@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <div class="row">
            <div class="col-md-4">
                <div class="card border-warning mb-3" style="max-width: 18rem;">
                    <div class="card-header bg-transparent border-warning">Job Details</div>
                    <div class="card-body text-warning">
                        <form class="mt-2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Category</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Category" value="{{$detail->category}}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Customer Name</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Customer Name" value-="{{$detail->customername}}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Email" value="{{$detail->customeremail}}" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Budget</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" value="{{$detail->budget}}"  placeholder="Budget" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Area</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" readonly>{{$detail->area}}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">City</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1" value="{{$detail->city}}"  placeholder="City" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Job Description</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" readonly>{{$detail->description}}</textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card border-info mb-3" style="max-width: 18rem;">
                    <div class="card-header bg-transparent border-info">Accepted Requests</div>
                    <div class="card-body text-info">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Supplier</th>
                                <th scope="col">Rating</th>
                                <th scope="col">Detail</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($acceptedRequests as $rowaccept)
                            <tr>
                                <td>{{$rowaccept->suppliername}}</td>
                                <td>@php
                                    if($rowaccept->supplierrating== 0){
                                   echo 'NA';
                                   }
                                    elseif($rowaccept->supplierrating==5 ){
                                   echo '*****';
                                   }
                                   elseif($rowaccept->supplierrating> 0 || $rowaccept->supplierrating< 2){
                                echo '*' ;
                                   }
                               elseif($rowaccept->supplierrating> 2|| $rowaccept->supplierrating< 3){
                                echo '**' ;
                                   }
                                elseif($rowaccept->supplierrating> 3|| $rowaccept->supplierrating< 4){
                                echo '***' ;
                                   }
                                   elseif($rowaccept->supplierrating> 4|| $rowaccept->supplierrating< 5){
                                echo '****' ;
                                   }

                                @endphp
                                <td><a href="{{route('userdetail', $rowaccept->supplierid)}}" target="_blank">View</a></td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="card border-success mb-3" style="max-width: 18rem;">
                    <div class="card-header bg-transparent border-success">Completed Requests</div>
                    <div class="card-body text-success">
                        <table class="table">
                            <thead>
                            <tr>
                                <th scope="col">Supplier</th>
                                <th scope="col">Rating</th>
                                <th scope="col">Detail</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php
                            $counter =0;
                            @endphp
                            @foreach($completedRequests as $rowcomplete)
                                <tr>
                                    <td>{{$rowcomplete->suppliername}}</td>
                                    <td>@php
                                            if($rowcomplete->supplierrating== 0){
                                           echo 'NA';
                                           }
                                            elseif($rowcomplete->supplierrating==5 ){
                                           echo '*****';
                                           }
                                           elseif($rowcomplete->supplierrating> 0 || $rowcomplete->supplierrating< 2){
                                        echo '*' ;
                                           }
                                       elseif($rowcomplete->supplierrating> 2|| $rowcomplete->supplierrating< 3){
                                        echo '**' ;
                                           }
                                        elseif($rowcomplete->supplierrating> 3|| $rowcomplete->supplierrating< 4){
                                        echo '***' ;
                                           }
                                           elseif($rowcomplete->supplierrating> 4|| $rowcomplete->supplierrating< 5){
                                        echo '****' ;
                                           }

                                        @endphp

                                    </td>
                                    <td><a data-toggle="modal" data-target="#reviewModal{{$counter}}" href="javascript:void(0);" target="_blank">View Review</a>
                                    </td>
                                </tr>
                                <div class="modal fade" id="reviewModal{{$counter}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog" role="document">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Review</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body">
                                                <div class="d-flex justify-content-center">
                                                    <div class="content text-center">
                                                        <div class="ratings">
                                                            <div class="stars">
                                                                @for($i=0;$i<$rowcomplete->rating;$i++)
                                                                    <i class="fa fa-star"></i>
                                                                @endfor
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="InputCategoryName">Review Content</label>
                                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="5" disabled>{{$rowcomplete->review}}</textarea>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @php
                                $counter++;
                                @endphp
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- Model Div  -->
@endsection
