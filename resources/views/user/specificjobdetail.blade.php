@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
                <div class="card border-success mb-3">
                    <div class="card-header bg-transparent border-success">Job Details</div>
                    <div class="card-body text-success">
                        <form class="mt-2">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Category</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Category" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Phone No</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Phone Number" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Customer Name</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Customer Name" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Email" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Budget</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Budget" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Area</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="Area" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">City</label>
                                        <input type="text" class="form-control" id="exampleInputEmail1"  placeholder="City" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="exampleFormControlTextarea1">Job Description</label>
                                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" readonly></textarea>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="card-footer bg-transparent border-success">
                        <button type="button" class="btn btn-primary">Completed</button>
                        <a href="{{route('userdetail')}}" class="btn btn-success">
                            Go To Customer Profile
                        </a>
                    </div>

                </div>
    </div>

@endsection
