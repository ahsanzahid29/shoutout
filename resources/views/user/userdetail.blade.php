@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
        <nav>
            <div class="nav nav-tabs" id="nav-tab" role="tablist">
                <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-overview" role="tab" aria-controls="nav-home" aria-selected="true">Overview</a>
                @if($userRole == 3)
                <a class="nav-item nav-link" id="nav-establishment-tab" data-toggle="tab" href="#nav-establishment" role="tab" aria-controls="nav-profile" aria-selected="false">Establishment</a>
                @endif
                <a class="nav-item nav-link" id="nav-inbox-tab" data-toggle="tab" href="#nav-inbox" role="tab" aria-controls="nav-contact" aria-selected="false">Inbox</a>
                <a class="nav-item nav-link" id="nav-settings-tab" data-toggle="tab" href="#nav-settings" role="tab" aria-controls="nav-contact" aria-selected="false">Settings</a>

            </div>
        </nav>
        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active mt-3" id="nav-overview" role="tabpanel" aria-labelledby="nav-home-tab">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="card mt-5">
                            <div class="card-body">
                                <h5 class="card-title">{{$user->name}}</h5>
                                <p class="card-text">@if($userRole == 2)(Customer) @elseif($userRole == 3) (Customer & Supplier) @endif</p>
                                <a href="javascript:void(0);" class="btn btn-primary">Signup Date: {{date('d M, Y',strtotime($user->created_at))}}</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-8">
                        @if($userRole == 2)
                        <p class="h1">Customer Stats</p>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $countmyJobs }}</h5>
                                        <p class="card-text">No of jobs</p>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $countRequestReceived }}</h5>
                                        <p class="card-text">Request Received</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $countRequestAwarded }}</h5>
                                        <p class="card-text">Request Awarded</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 mt-2">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$countCustomerOpenJobs}}</h5>
                                        <p class="card-text">Open Jobs</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4 mt-2">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">{{$countCustomerCompletedJobs}}</h5>
                                        <p class="card-text">Completed Jobs</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endif
                        @if($userRole == 3)
                        <p class="h1">Supplier Stats</p>
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $myCredits->noofcredits }}</h5>
                                        <p class="card-text">Credits Purchased</p>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $totalReviews }}</h5>
                                        <p class="card-text">Total Reviews</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-2">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $awardedJobs }}</h5>
                                        <p class="card-text">Awarded Jobs</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6 mt-2">
                                <div class="card">
                                    <div class="card-body">
                                        <h5 class="card-title">{{ $completedJobs }}</h5>
                                        <p class="card-text">Completed Jobs</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                         @endif
                    </div>
                </div>
            </div>
            @if($userRole == 3)
            <div class="tab-pane fade" id="nav-establishment" role="tabpanel" aria-labelledby="nav-profile-tab">

                       <div class="row">
                       <div class="col-md-6">
                           <div class="form-group">
                               <label for="exampleInputEmail1">Name</label>
                               <input type="text" class="form-control" id="exampleInputEmail1" value="{{$supplierEstablishmentDetail->suppliername}}"  placeholder="Enter Name" disabled>
                           </div>
                       </div>
                       <div class="col-md-6">
                           <div class="form-group">
                               <label for="exampleInputEmail1">Email</label>
                               <input type="email" class="form-control" id="exampleInputEmail1" value="{{$supplierEstablishmentDetail->supplieremail}}"  placeholder="Enter Email" disabled>
                           </div>
                       </div>
                       </div>
                       <div class="row">
                           <div class="col-md-6">
                               <div class="form-group">
                                   <label for="exampleInputEmail1">Phone</label>
                                   <input type="text" class="form-control" id="exampleInputEmail1" value="{{$supplierEstablishmentDetail->suppliercontact}}"  placeholder="Enter Phone Number" disabled>
                               </div>
                           </div>
                           <div class="col-md-6">
                               <div class="form-group">
                                   <label for="exampleInputEmail1">Province</label>
                                   <input type="text" class="form-control" id="exampleInputEmail1" value="{{$supplierEstablishmentDetail->regionname}}"  placeholder="Province" disabled>
                               </div>
                           </div>
                       </div>
                       <div class="row">
                           <div class="col-md-6">
                               <div class="form-group">
                                   <label for="exampleInputEmail1">Sub Category</label>
                                   <input type="text" class="form-control" id="exampleInputEmail1" value="{{$supplierEstablishmentDetail->suppliercategory}}"  placeholder="Sub Category" disabled>
                               </div>
                           </div>
                           <div class="col-md-6">
                               <div class="form-group">
                                   <label for="exampleInputEmail1">Address</label>
                                   <textarea class="form-control" rows="3" cols="3" disabled>{{$supplierEstablishmentDetail->supplieraddress}}</textarea>
                               </div>
                           </div>
                       </div>
                       <div class="row">
                           <div class="col-md-12">
                               <div class="form-group">
                                   <label for="exampleFormControlTextarea1">About Establishment</label>
                                   <textarea class="form-control" id="exampleFormControlTextarea1" rows="3" disabled>{{$supplierEstablishmentDetail->aboutsupplier}}</textarea>
                               </div>
                           </div>
                       </div>
                @if(count($supplierImages)>0)

                <div class="mt-3"></div>
                <p class="h4">Establishment Images</p>
                <table class="table" id="establishmentimagestable">
                    <thead>
                    <tr>
                        <th scope="col">Image</th>
                        <th scope="col">Uploaded at</th>
                        <th scope="col">Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($supplierImages as $row)
                        <tr>
                            <td><img src ="{{ Storage::url($row->image) }}" alt="Establishment Image" width="50" height="50"/></td>
                            <td>{{ date ('d M, Y',strtotime($row->created_at)) }}</td>
                            <td>
                                <a href="{{route('deleteestablishmentimagebyadmin' , $row->id)}}" onclick="return confirm('Are you sure you want to delete this image?');" class="btn btn-danger btn-sm">Delete</a>
                            </td>

                        </tr>
                    @endforeach

                    </tbody>
                </table>
                    @endif
            </div>
            @endif
            <div class="tab-pane fade mt-3" id="nav-inbox" role="tabpanel" aria-labelledby="nav-inbox-tab">
                @if($userRole == 2)
                <p class="h2">Customer Inbox</p>
                <div class="mt-2"></div>

                <table class="table" id="customerinboxtable">
                    <thead>
                    <tr>
                        <th scope="col">Job Description</th>
                        <th scope="col">Created At</th>
                        <th scope="col">Status</th>
                        <th scope="col">Detail</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($myJobs as $row)
                        <tr>
                            <th scope="row"><span class="d-inline-block text-truncate" style="max-width: 150px;">{{ $row->description }}</span></th>
                            <td>{{ date ('d M ,Y',strtotime($row->created_at)) }}</td>
                            <td>@php
                                if($row->status==1){
                            echo '<span class="badge badge-success">Open</span>';

                            }
                            else{

                            }
                            @endphp
                            </td>
                            <td><a target="_blank" href="{{route('customerjob', $row->id)}}">View Job</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                @endif
                @if($userRole == 3)
                <div class="mt-5"></div>
                <p class="h2">Supplier Inbox</p>
                <div class="mt-2"></div>
                <table class="table" id="supplierinboxtable">
                    <thead>
                    <tr>
                        <th scope="col">Job Description</th>
                        <th scope="col">Job Date</th>
                        <th scope="col">Status</th>
                        <th scope="col">Detail</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($supplierjobs as $rowsupplier)
                        <tr>
                            <th scope="row"><span class="d-inline-block text-truncate" style="max-width: 150px;">{{$rowsupplier->jobdescription}}</span></th>
                            <td>{{ date('d M, Y', strtotime($rowsupplier->jobdate)) }}</td>
                            <td>@if($rowsupplier->jobstatus==0)<span class="badge badge-danger">Pending</span>@elseif($rowsupplier->jobstatus==1)<span class="badge badge-info">Accepted</span>@elseif($rowsupplier->jobstatus==2)<span class="badge badge-success">Awarded</span>@elseif($rowsupplier->jobstatus==3)<span class="badge badge-warning">Request For Review</span>@elseif($rowsupplier->jobstatus==4)<span class="badge badge-success">Reviewed</span>@endif</td>
                            <td><a target="_blank" href="{{route('supplierjob', $rowsupplier->jobid)}}">View Job</a></td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                    @endif
            </div>
            <div class="tab-pane fade" id="nav-settings" role="tabpanel" aria-labelledby="nav-settings-tab">
                <form class="mt-3" method="post" action="{{route('updateuser')}}" enctype="multipart/form-data">
                    <input type="hidden" name="user_id" value="{{$user->id}}" />
                    @csrf
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Name</label>
                                <input type="text" name="name" class="form-control" id="exampleInputEmail1" value="{{$user->name}}"  placeholder="Enter Name" >
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email"  class="form-control" id="exampleInputEmail1" value="{{$user->email}}" readonly  placeholder="Enter Email" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Password</label>
                                <input name="password" type="text" class="form-control" id="exampleInputEmail1" value="{{$decrypted}}"  placeholder="****">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Profile Picture</label>
                                <input name="userimage" type="file" class="form-control-file" id="exampleFormControlFile1">
                            </div>
                        </div>
                        @if($user->profileimage==null)

                            @else
                        <div class="col-md-2">
                            <img src="{{ Storage::url($user->profileimage) }}" width="100px" height="100px" />
                        </div>
                            @endif

                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="InputCategoryImage">Status</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="status" id="active" value="true" @if($user->status== 'true') checked @endif>
                                    <label class="form-check-label" for="active">Active</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="status" id="inactive" value="false" @if($user->status== 'false') checked @endif>
                                    <label class="form-check-label" for="inactive">In-active</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Update</button>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready( function () {
            $('#establishmentimagestable,#customerinboxtable,#supplierinboxtable').DataTable();
        } );
    </script>
@endsection
