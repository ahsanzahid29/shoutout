@extends('layouts.sidebar')
@section('content1')
    <div class="col-md-9">
    <nav>
        <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">About Us</a>
            <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Terms & Conditions</a>
            <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">Extras</a>
        </div>
    </nav>
    <div class="tab-content" id="nav-tabContent">
        <div class="tab-pane fade show active mt-1" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
            <form method="post" action="{{route('updateaboutus')}}">
                <input type="hidden" name="recordid" value="{{$cms['id']}}" />
                @csrf
                <div class="form-group mt-2">
                    <label for="InputCategoryName">About Video</label>
                    <input type="text" class="form-control @error('aboutusvideo') is-invalid @enderror" name="aboutusvideo" id="InputCategoryName" placeholder="Give Vimeo Link" value="{{$cms['aboutusvideo']}}">
                    @error('aboutusvideo')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <div class="form-group ">
                    <label for="exampleFormControlTextarea1">Description</label>
                    <textarea class="form-control @error('aboutusdescription') is-invalid @enderror" name="aboutusdescription" id="exampleFormControlTextarea1" rows="3">{{$cms['aboutusdescription']}}</textarea>
                    @error('aboutusdescription')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Update About Us</button>
            </form>
        </div>
        <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
            <form method="post" action="{{route('updatetermsandcondition')}}">
                <input type="hidden" name="recordid" value="{{$cms['id']}}}">
                @csrf
            <div class="form-group mt-2">
                <label for="InputCategoryName">Terms & Condition Video</label>
                <input type="text" class="form-control @error('termsvideo') is-invalid @enderror" id="InputCategoryName" name="termsvideo" placeholder="Give Vimeo Link" value="{{$cms['termsvideo']}}">
                @error('termsvideo')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
            <div class="form-group">
                <label for="exampleFormControlTextarea1">Description</label>
                <textarea class="form-control @error('termsdescription') is-invalid @enderror" id="exampleFormControlTextarea1" name="termsdescription" rows="3">{{$cms['termsdescription']}}</textarea>
                @error('termsdescription')
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                @enderror
            </div>
                <button type="submit" class="btn btn-success">Update Terms & Conditions</button>
            </form>
        </div>
        <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
            <form method="post" action="{{route('updatewelcomevideo')}}">
                <input type="hidden" name="recordid" value="{{$cms['id']}}" />
                @csrf
                <div class="form-group mt-2">
                    <label for="InputCategoryName">Welcome Video</label>
                    <input type="text" class="form-control @error('extravideo') is-invalid @enderror" id="InputCategoryName" name="extravideo" placeholder="Give Vimeo Link" value="{{$cms['extravideo']}}">
                    @error('extravideo')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-success">Update</button>
            </form>
        </div>
    </div>
    </div>
@endsection
